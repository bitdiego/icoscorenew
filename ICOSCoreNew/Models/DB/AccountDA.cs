﻿using ICOSCoreNew.Models;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Session;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace ICOSCoreNew.DB
{
    public class AccountDA : IUserLoginServices
    {
       // private readonly IUserAccountServices _accountService;
        private readonly ICOSEntities dbContext;
        
        public AccountDA(/*IUserAccountServices accountService,*/ ICOSEntities dbContext)
        {
           // _accountService = accountService;
            this.dbContext = dbContext;
        }

        public static int CurrentUserID { get; set; }
        public static int CurrentICOS { get; set; }
        public static string CurrentUserName { get; set; }

       

        /*public static string CurrentUserName
        {
            get
            {
                string username = "";
                if (_handler.Context.Session != null && _handler.Context.Session.GetString("userName") != null)
                {
                    username = _handler.Context.Session.GetString("userName").ToString();
                }
                return username;
            }
            set
            {
                //if (_handler.Context.Session != null)
                 //   _handler.Context.Session.SetString("userName", value);
            }
        }*/
        /*
        public static int CurrentICOS
        {
            get
            {
                int ICOS = 0;
                if (_handler.Context.Session != null && _handler.Context.Session.GetString("ICOS") != null)
                    ICOS = _handler.Context.Session.GetString("ICOS").ToString() == "1" || _handler.Context.Session.GetString("ICOS").ToString().ToLower() == "1" ? 1 : 0;
                    
                return ICOS;
            }
            set
            {
               
                if (_handler.Context.Session != null)
                    _handler.Context.Session.SetInt32("ICOS", value);
                    
            }
        }*/

        public UserModel Login(LoginModel model)
        {
            
            UserModel user = null;
            SqlParameter username = new SqlParameter("@username", model.UserName);
            SqlParameter password = new SqlParameter("@pass", model.Password);
            var result = dbContext.Users.FromSql("CheckLogin", parameters: new[] { "@username", model.UserName, "@pass", model.Password });

            // var result = dbContext
            //     .ToList();
            /* SqlParameter output = new SqlParameter("@userId", SqlDbType.Int);
             output.Direction = ParameterDirection.Output;
             string username = DB.qts(model.UserName);
             string password = DB.qts(model.Password);
             SqlParameterCollection result = db.ExecuteStoredProcedure("[dbo].[CheckLogin]"
                 , new SqlParameter("@username", username)
                 , new SqlParameter("@pass", password)
                 , output);

             string userid = result["@userId"].Value.ToString();
             if (!String.IsNullOrEmpty(userid))
             {
                 var data = db.ExecuteReader("SELECT first_acc FROM dbo.Users WHERE id_users=" + userid);
                 data.Read();
                 acc = int.Parse(data["first_acc"].ToString());
                 data.Close();

                 user = new UserModel();
                 user.userName = username;
                 user.userID = int.Parse(userid);
                 user.firstAccess = acc == 0 ? false : true;
             }
             db.Dispose();*/
            return user;
        }

        public async Task<UserModel> CheckLoginAsync(LoginModel model)
        {

            UserModel _user;
            
            SqlParameter password = new SqlParameter("@pass", model.Password);
            SqlParameter username = new SqlParameter("@username", model.UserName);
            SqlParameter userId = new SqlParameter
            {
                ParameterName = "@userId",
                SqlDbType = SqlDbType.Int,
                Direction = ParameterDirection.Output
            };
            object[] parameters =
            {
               username, password, userId
            };

            await dbContext.Database.ExecuteSqlCommandAsync("EXEC CheckLogin @username, @pass, @userId OUT", parameters);
            try
            { 
                int _userId=(int)userId.Value;
                _user = dbContext.Users.SingleOrDefault(user => user.id_users == _userId);
                CurrentUserID = _userId;
                CurrentICOS = 1;
                CurrentUserName = model.UserName;
            }
            catch(Exception e)
            { 
                return null;
            }

            return _user;
        }

        /* public static UserModel Login(LoginModel model, DB db = null)
         {
             db = new DB();
             int acc;
             UserModel user = null;
             SqlParameter output = new SqlParameter("@userId", SqlDbType.Int);
             output.Direction = ParameterDirection.Output;
             string username = DB.qts(model.UserName);
             string password = DB.qts(model.Password);
             SqlParameterCollection result = db.ExecuteStoredProcedure("[dbo].[CheckLogin]"
                 , new SqlParameter("@username", username)
                 , new SqlParameter("@pass", password)
                 , output);

             string userid = result["@userId"].Value.ToString();
             if (!String.IsNullOrEmpty(userid))
             {
                 var data = db.ExecuteReader("SELECT first_acc FROM dbo.Users WHERE id_users=" + userid);
                 data.Read();
                 acc = int.Parse(data["first_acc"].ToString());
                 data.Close();

                 user = new UserModel();
                 user.userName = username;
                 user.userID = int.Parse(userid);
                 user.firstAccess = acc == 0 ? false : true;
             }
             db.Dispose();
             return user;
         }
         */
        public void ChangePassword(ChangePasswordModel model)
        {

        }
        /*
        public static void ChangePassword(ChangePasswordModel model, DB db = null)
        {
            db = new DB();
            string body = "", umail = "";
            string password = DB.qts(model.Password);
            string userID = DB.qts(model.userID.ToString());

            db.ExecuteStoredProcedure("[dbo].[UpdateUserPass]"
                , new SqlParameter("@userId", userID)
                , new SqlParameter("@newpass", password));

            var data = db.ExecuteReader("SELECT[Name],[Email]  FROM[dbo].[Users] WHERE id_users = " + userID);
            try
            {
                if (data.HasRows)
                {
                    data.Read();

                    umail = data["Email"].ToString();
                    body += Environment.NewLine + "Password changed for user " + data["Name"].ToString() + ", email " + umail;

                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    message.To.Add(umail);
                    //message.Bcc.Add("database@unitus.it");
                    message.Bcc.Add(umail);
                    message.Subject = "Password changed for ICOS user";
                    //message.From = new System.Net.Mail.MailAddress("database@unitus.it");
                    message.From = new System.Net.Mail.MailAddress("emmy.jacobs@uantwerpen.be");
                    message.Body = body;

                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new System.Net.NetworkCredential("database@unitus.it", "ICOS2014etc");
                    smtp.Send(message);
                }
            }
            catch (Exception ee)
            {
                body += Environment.NewLine + "Error in Password change for user " + data["Name"].ToString();
                body += Environment.NewLine + ee.ToString();

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                //message.To.Add("diego.p@unitus.it");
                //message.Bcc.Add("database@unitus.it ");
                message.Subject = "Password changed for ICOS user - Error";
                //message.From = new System.Net.Mail.MailAddress("database@unitus.it");
                message.From = new System.Net.Mail.MailAddress("emmy.jacobs@uantwerpen.be");
                message.Body = body;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential("database@unitus.it", "ICOS2014etc");
                smtp.Send(message);
            }
            data.Close();
            db.Dispose();
        }
        */
        public ResetPasswordModel ResetPassword(ResetPasswordModel model)
        {
            return model;
        }
        /*    
        public static ResetPasswordModel ResetPassword(ResetPasswordModel model, DB db = null)
        {
            db = new DB();
            int userID;
            string userName;
            string chars = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789_@";
            string newPass = "";
            string body = "";
            string userMail = DB.qts(model.email);

            var data = db.ExecuteReader("SELECT [id_users], [Name] FROM [dbo].[Users] WHERE Email='" + userMail + "'");

            if (data.HasRows)
            {
                model.existingEmail = true;
                data.Read();
                userID = data.GetInt32(0);
                userName = data.GetString(1);
                data.Close();

                //generate random password
                Random rnd = new Random();
                for (int i = 0; i < 8; i++)
                {
                    int ii = rnd.Next(0, chars.Length);
                    newPass += chars.ElementAt(ii);
                }

                db.ExecuteStoredProcedure("ResetUserPass"
                    , new SqlParameter("@userId", userID)
                    , new SqlParameter("@newpass", newPass)
                    , new SqlParameter("@facc", 1));

                try
                {
                    body += Environment.NewLine + "User " + userName + " has requested a password reset.";
                    body += Environment.NewLine + "Temporary pass: " + newPass;
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    //message.To.Add("diego.p@unitus.it");
                    //message.Bcc.Add("database@unitus.it");
                    message.Bcc.Add(userMail);
                    message.Subject = "Password reset for ICOS user";
                    //message.From = new System.Net.Mail.MailAddress("database@unitus.it");
                    message.From = new System.Net.Mail.MailAddress("emmy.jacobs @uantwerpen.be");
                    message.Body = body;

                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new System.Net.NetworkCredential("database@unitus.it", "ICOS2014etc");
                    smtp.Send(message);
                }
                catch (Exception ee)
                {
                    body += Environment.NewLine + "Error in Password change for user " + userName;
                    body += Environment.NewLine + ee.ToString();
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    //message.To.Add("diego.p@unitus.it");
                    //message.Bcc.Add("database@unitus.it ");
                    message.Subject = "Password reset for ICOS user - Error";
                    //message.From = new System.Net.Mail.MailAddress("database@unitus.it");
                    message.From = new System.Net.Mail.MailAddress("emmy.jacobs @uantwerpen.be");
                    message.Body = body;

                    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new System.Net.NetworkCredential("database@unitus.it", "ICOS2014etc");
                    smtp.Send(message);
                }
            }
            else
                model.existingEmail = false;

            db.Dispose();
            return model;
        }*/
    }
}