﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.ViewModel.BADM
{
    public class LocationGroupViewModel
    {
        public string LOCATION_LAT { get; set; }
        public string LOCATION_LONG { get; set; }
        public string LOCATION_ELEV { get; set; }
        public string LOCATION_DATE { get; set; }
        public string LOCATION_COMMENT { get; set; }
        public string SITE { get; set; }  // TODO: AC - ok nel filtro o disturba o in una base class !!??? 
        public int SiteId { get; set; }
    }
}
