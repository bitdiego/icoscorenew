﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.ViewModel
{
    public class SubmittedFilesViewModel
    {
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string Submitter { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Link { get; set; }
        public Nullable<DateTime> SumissionDate { get; set; }
    }
}
