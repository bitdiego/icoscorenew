﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.ViewModel
{
    public class SiteViewModel
    {
        public int siteID { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public string NLat { get; set; }
        public string NLon { get; set; }
        public short Datastatus { get; set; }
    }
}
