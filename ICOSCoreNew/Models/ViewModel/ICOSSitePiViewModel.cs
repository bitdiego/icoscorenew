﻿using ICOSCoreNew.Models.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.ViewModel
{
    public class ICOSSitePiViewModel
    {
        public string SiteId { get; set; }
        public List<SelectListItem> sitesPiList;
    }
}
