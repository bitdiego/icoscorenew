﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.ViewModel
{
    public class FieldBookViewModel
    {
        public string OriginalName { get; set; }
        public string DataInfo { get; set; }
        public string Content { get; set; }
        public Nullable<DateTime> ImportDate { get; set; }
        public string SubmitterName { get; set; }
    }
}
