﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.DTO
{
    interface IAdapter<TSource, TDest>
    {
        TDest Adapt(TSource source);
        TDest Adapt(TSource source, int siteId);
        List<TDest> Adapt(List<TSource> source);
    }
}
