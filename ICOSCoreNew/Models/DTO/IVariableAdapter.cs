﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.DTO
{
    public interface IVariableAdapter<T>
    {
        T GetVariableFromDataStorage(int siteId, int groupId);
        List<T> GetVariableListFromDataStorage(int siteId, int groupId);
    }
}
