﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.Entities
{
    public class Variable
    {
        public int id_variable { get; set; }
        public string Name { get; set; }
        public string unit_of_measure { get; set; }
        public short? unit_type { get; set; }
        public int group_id { get; set; }
        public int? cv_index { get; set; }
        public bool? required { get; set; }
        public string varIndex { get; set; }
        public bool? hasMultiple { get; set; }
        public string description { get; set; }
        public byte? srequired { get; set; }

        public virtual Group Group { get; set; }
    }
}
