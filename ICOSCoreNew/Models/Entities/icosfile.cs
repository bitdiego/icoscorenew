﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.Entities
{
    public class icosfile
    {
        [Key]
        public int ID { get; set; }
        public string nome { get; set; }
        public string data_info { get; set; }
        public Nullable<int> idins { get; set; }
        public Nullable<int> data_type { get; set; }
        public Nullable<DateTime> date { get; set; }
        public Nullable<int> site { get; set; }
        public Nullable<bool> imported { get; set; }
        public string description { get; set; }
        public string separator { get; set; }
        public Nullable<bool> compressed { get; set; }
        public string originalname { get; set; }
        public Nullable<int> year { get; set; }
        public Nullable<int> project { get; set; }
        public string status { get; set; }
        public Nullable<int> id_frequency { get; set; }
    }
}
