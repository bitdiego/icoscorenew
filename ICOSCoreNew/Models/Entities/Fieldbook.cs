﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ICOSCoreNew.Models.Entities;

namespace ICOSCoreNew.Models.Entities
{
    public class Fieldbook
    {
        [Key]
        public int id_fieldbook { get; set; }
        public string originalname { get; set; }
        public string data_info { get; set; }
        public int idins { get; set; }
        public int siteId { get; set; }
        public Nullable<int> data_type { get; set; }
        public string date { get; set; }
        public Nullable<DateTime> importDate { get; set; }
        public Nullable<DateTime> deletedDate { get; set; }
        public Nullable<short> status { get; set; }

        //public UserModel User { get; set; }
    }
}
