﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.Entities
{
    public class ICOS_site
    {
        public int id_icos_site { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public short? @class { get; set; }
    }
}
