using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ICOSCoreNew.Models.Entities
{
    public partial class FileType
    {
        [Key]
        public int id_filetype { get; set; }
        public string type { get; set; }
        public string description { get; set; }
    }
}
