﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ICOSCoreNew.Models.Entities
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [ValidPassword("Please enter a valid password:  at least 8 characters long with lowercase and uppercase alphabetic characters, digits and special characters")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public int userID { get; set; }
    }

    public class ValidPassword : ValidationAttribute
    {
        public ValidPassword(string errorMessage)
            : base(errorMessage)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ValidationResult validationResult = ValidationResult.Success;
            //string regex = "/^ (?=.*[a - z])(?=.*[A - Z])(?=.*\\d)(?=.*[$@$!% &_])[A - Za - z\\d$@$!% &_]{ 8,}/";
           
            try
            {
                if (value != null)
                {
                    String toValidate = (String)value;
                    Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%&_])[A-Za-z\d$@$!%&_]{8,}");
                    Match match = regex.Match(toValidate);
                    if (!match.Success)
                    {
                        validationResult = new ValidationResult(ErrorMessageString);
                    }
                }
                else
                    validationResult = new ValidationResult(ErrorMessageString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return validationResult;
        }
    }

    public class ResetPasswordModel
    {
        [Required]
        [DisplayName("Email")]
        //[ValidEmailAddress("Please enter a valid mail address")]
        public string email { get; set; }

        public bool? existingEmail { get; set; }
    }


}