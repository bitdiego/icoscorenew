﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICOSCoreNew.Models.UI;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.ViewModel;

namespace ICOSCoreNew.Models.Entities
{
    public class ICOSEntities : DbContext
    {
        public ICOSEntities(DbContextOptions<ICOSEntities> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //throw new UnintentionalCodeFirstException();
            modelBuilder.Entity<Group>().HasKey(o => o.id_group);
            modelBuilder.Entity<Variable>().HasKey(o => o.id_variable);
            modelBuilder.Entity<ICOS_site>().HasKey(o => o.id_icos_site);
            modelBuilder.Entity<SiteViewModel>().HasKey(o => o.siteID);
            modelBuilder.Entity<IcosSitePI>().HasKey((u => new
            {
                u.id_users,
                u.id_site
            }));
        }

        public virtual DbSet<ICOS_site> ICOS_site { get; set; }
        public virtual DbSet<SiteViewModel> vIcos_site { get; set; }
        public virtual DbSet<Variable> Variables { get; set; }
        public virtual DbSet<UserModel> Users { get; set; }
        public DbSet<IcosSitePI> IcosSitePIs { get; set; }
        public DbSet<SelectorVM> SelectorVM { get; set; }
        public virtual DbSet<DataStorage> DataStorages { get; set; }
        public virtual DbSet<icosfile> icosfiles { get; set; }
        public virtual DbSet<Fieldbook> Fieldbooks { get; set; }
        public virtual DbSet<FileType> FileTypes { get; set; }
        //public virtual DbSet<Group> Groups { get; set; }
        //public virtual DbSet<VarAttribute> VarAttributes { get; set; }
        //public virtual DbSet<IcosSitePI> IcosSitePIS { get; set; }
        //
        //
        //public virtual DbSet<User> Users { get; set; }


        //public virtual DbSet<InstrumentViewExt> InstrumentViewExts { get; set; }
        //public virtual DbSet<BADMList> BADMLists { get; set; }
    }
}
