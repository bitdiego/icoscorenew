﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace ICOSCoreNew.Models.Entities
{
    public class UserModel
    {
        [Key]
        public int id_users { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string Pwd { get; set; }

        public string Type { get; set; }

        public Int16 first_acc { get; set; }

        
    }
}