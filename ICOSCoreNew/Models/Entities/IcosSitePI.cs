﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.Entities
{
    public class IcosSitePI
    {
        public int id_users { get; set; }
        public int id_site { get; set; }
        public string  Name { get; set; }
        public string Email { get; set; }
        public string site_code { get; set; }
        public string type { get; set; }

        public IcosSitePI()
        {
        //    SiteListPIs = new List<IcosSitePI>();
        }
        //public List<IcosSitePI> SiteListPIs { get; set; }

    }
}

