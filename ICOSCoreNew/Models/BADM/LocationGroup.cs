﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.BADM
{
    public class LocationGroup
    {
        public const int GROUP_ID = 3;
       // newIcos.DB.DB db;
        public string LOCATION_LAT { get; set; }
        public string LOCATION_LONG { get; set; }
        public string LOCATION_ELEV { get; set; }
        public string LOCATION_DATE { get; set; }
        public string LOCATION_COMMENT { get; set; }
    }
}
