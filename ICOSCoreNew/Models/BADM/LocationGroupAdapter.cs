﻿using ICOSCoreNew.Models.DTO;
using ICOSCoreNew.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.BADM
{
    public class LocationGroupAdapter : IVariableAdapter<LocationGroup>
    {
        private readonly ICOSEntities dbContext;

        public LocationGroupAdapter(ICOSEntities _dbc)
        {
            this.dbContext = _dbc;
        }

        public LocationGroup GetVariableFromDataStorage(int siteId, int groupId)
        {
            SqlParameter sid = new SqlParameter("@siteId", siteId);
            SqlParameter grid = new SqlParameter("@grId", groupId);
            object[] pars = {sid, grid };
            //var result = dbContext.DataStorages.FromSql("SelectDataGeneralUI", parameters: new[] { "@siteId", 70,  }).SingleOrDefault();
            var result = dbContext.DataStorages.FromSql("EXEC SelectDataGeneralUI @siteId, @grId", sid, grid);
            DataStorage d = (DataStorage)result;
            
            LocationGroup lgr = new LocationGroup();
            //lgr.LOCATION_LAT=result.Provider.
            return lgr;
        }
        public List<LocationGroup> GetVariableListFromDataStorage(int siteId, int groupId)
        {
            throw new NotImplementedException();
        }
    }
}
