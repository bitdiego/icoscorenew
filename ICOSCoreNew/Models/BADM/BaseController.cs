﻿using ICOSCoreNew.DB;
using ICOSCoreNew.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.BADM
{
    [Serializable]
    public class OperationResponse
    {
        public int code { get; set; }
        public string message { get; set; }
    }


    public class BaseController : Controller
    {
        protected const string OPERATION_UPDATE = "Update";
        protected const string OPERATION_ADD = "Create";
        protected const string OPERATION_DELETE = "Delete";
        public JsonResult Create<M, A, U>(U ui, int siteid, string validationmembername, int groupid)
                                    where M : class, new()
                                    where A : class, new()
                                    where U : class
        {
            OperationResponse res = new OperationResponse { code = 0, message = "Add complete" };
            if (this.ModelState.IsValid)
            {
                try
                {
                    var model = Activator.CreateInstance<M>();
                    var adapter = Activator.CreateInstance<A>();
                    var servervalidator = Activator.CreateInstance(typeof(ServerValidator));
                    //
                    MethodInfo mi = servervalidator.GetType().GetMethod(validationmembername);
                    string validateReturn = "Ok";
                    if (null != mi) validateReturn = (string)mi.Invoke(servervalidator, new object[] { ui });
                    if (validateReturn.Length == 0)
                    {
                        model.GetType().GetMethod("Add").Invoke(model, new object[]{
                    adapter.GetType().GetMethod("Adapt",new Type[]{typeof(U)}).Invoke(adapter,new object[]{ui})
                    ,AccountDA.CurrentUserID
                    ,siteid
                    ,groupid
                  });
                    }
                    else
                    {
                        res.code = 200;
                        res.message = validateReturn;
                    }
                }
                catch (Exception ex)
                {
                    res.code = 200;
                    res.message = "Database error";
                }
                return Json(res);
            }
            else
            {
                res.code = 100;
                res.message = "Model is not valid";
            }
            return Json(res);
        }
        public JsonResult getData<M, A, F, U>(F filter) where M : class, new()
                                                    where A : class, new()
                                                    where F : class, new()
                                                    where U : class, new()
        {
            try
            {
                string site = "";//System.Web.HttpContext.Current.Session["PiSite"].ToString();
                if (string.IsNullOrEmpty(site)) site = "-1";
                filter.GetType().GetProperty("SITE").SetValue(filter, site);
            }
            catch (Exception ex)
            {
                var err = ex.ToString();
                // TODO: AC - what to do ??
            }
            var model = Activator.CreateInstance<M>();
            var adapter = Activator.CreateInstance<A>();
            var resList = adapter.GetType().GetMethod("Adapt", new Type[] { typeof(List<M>) }).Invoke(adapter, new object[] { model.GetType().GetMethod("GetData", BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Invoke(model, new object[] { filter }) }) as List<U>;
            // filters
            List<string> xparm = new List<string>();
            Type myType = filter.GetType();
            IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
            int contaparms = 0;
            string xdynque = @"";
            foreach (PropertyInfo prop in props)
            {
                if (prop.Name != "SITE")
                {
                    object propValue = prop.GetValue(filter, null);
                    if (propValue != null)
                    {
                        // add to filer string
                        //xdynque = prop.Name.Replace("_DESCRIPTION", "") + @".Contains(@" + contaparms.ToString() + ") AND ";
                        xdynque = xdynque + prop.Name + @".Contains(@" + contaparms.ToString() + ") AND ";
                        // 
                        xparm.Add(propValue.ToString());

                        contaparms++;
                    }
                }

                // Do something with propValue
            }


            if (xparm.Count > 0)
            {
                xdynque = xdynque.Substring(0, xdynque.Length - 5);
                //resList=resList.Where(xdynque)
                //resList = resList.Where(xdynque, xparm.ToArray()).ToList();
            }
            return Json(resList);
        }
        public JsonResult Edit<M, A, U>(U ui, string validationmembername, int groupid) where M : class where A : class, new() where U : class
        {
            OperationResponse res = new OperationResponse { code = 0, message = "" };
            if (ModelState.IsValid)
            {
                try
                {
                    int xsite = 0;
                    // TODO: DIEGO_____ check site ( refactor to function..)
                   /* if (System.Web.HttpContext.Current.Session["PiSite"] != null)
                    {
                        int.TryParse(System.Web.HttpContext.Current.Session["PiSite"].ToString(), out xsite);
                    }*/
                    if (xsite > 0)
                    {
                        var adapter = Activator.CreateInstance<A>();
                        var model = Activator.CreateInstance<M>();
                        model = adapter.GetType().GetMethod("Adapt", new Type[] { typeof(U) }).Invoke(adapter, new object[] { ui }) as M;
                        var servervalidator = Activator.CreateInstance(typeof(ServerValidator));
                        //
                        MethodInfo mi = servervalidator.GetType().GetMethod(validationmembername);
                        string validateReturn = "Ok";
                        if (null != mi) validateReturn = (string)mi.Invoke(servervalidator, new object[] { model });
                        if (validateReturn.Length == 0)
                        {
                            model.GetType().GetMethod(OPERATION_UPDATE).Invoke(model, new object[]{
                    adapter.GetType().GetMethod("Adapt",new Type[]{typeof(U)}).Invoke(adapter,new object[]{ui})
                    ,AccountDA.CurrentUserID
                    ,xsite
                    ,groupid
                  });
                        }
                        else
                        {
                            res.code = 200;
                            res.message = validateReturn;
                        }
                    }
                    else
                    {
                        res.code = 300;
                        res.message = "Please select a valid site";
                    }
                }
                catch (Exception ex)
                {
                    res.code = 500;
                    res.message = "Unable to edit values";
                }
            }
            else
            {
                res.code = 50;
                res.message = "Please check invalid fields";
            }
            return Json(res);
        }
        public JsonResult Delete<M, A, U>(U ui) where M : class, new() where A : class, new() where U : class
        {
            OperationResponse res = new OperationResponse { code = 0, message = "Record deleted" };
            try
            {
                var model = Activator.CreateInstance<M>();
                var adapter = Activator.CreateInstance<A>();
                model.GetType().GetMethod(OPERATION_DELETE).Invoke(model, new object[]{
               adapter.GetType().GetMethod("Adapt",new Type[]{typeof(U)}).Invoke(adapter,new object[]{ui})
              ,AccountDA.CurrentUserID
            });
            }
            catch (Exception ex)
            {
                res.code = ex.HResult;
                res.message = "Unable to delete item.Please contact help desk.";
            }
            return Json(res);
        }
    }
}
