﻿/*using Microsoft.AspNetCore.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
*/
namespace ICOSCoreNew.Models.Utils
{
    public class Globals
    {
        public enum BadmVarsGroups
        {
            GRP_HEADER = 1,
            GRP_TEAM,
            GRP_LOCATION,
            GRP_UTC_OFFSET,
            GRP_LAND_OWNERSHIP,
            GRP_TOWER,
            GRP_CLIM_AVG,
            GRP_DM,
            GRP_PLOT,
            GRP_FLSM,
            GRP_SOSM,
            GRP_DHP,
            GRP_GAI,
            GRP_CEPT,
            GRP_BULKH,
            GRP_SPP,
            GRP_TREE,
            GRP_AGB,
            GRP_LITTER,
            GRP_ALLOM,
            GRP_WTD,
            GRP_D_SNOW,
            GRP_INST = 2000,
            GRP_LOGGER,
            GRP_FILE,
            GRP_EC,
            GRP_ECSYS,
            GRP_BM,
            GRP_STO,
            GRP_CHEMICAL_DATA = 2999,
            GRP_SPP_1 = 3000,
            GRP_LAI,
            GRP_BIOMASS,
            GRP_HEIGHTC,
            GRP_SA,
            GRP_DBH,
            GRP_BASAL_AREA,
            GRP_TREES_NUM,
            GRP_ROOT_DEPTH,
            GRP_PHEN_EVENT_TYPE,
            GRP_IGBP,
            GRP_RESEARCH_TOPIC,
            GRP_SITE_FUNDING,
            GRP_URL,
            GRP_ACKNOWLEDGEMENT,
            GRP_FLUX_MEASUREMENTS,
            GRP_SITE_CHAR1,
            GRP_SITE_CHAR2,
            GRP_SITE_CHAR3,
            GRP_SITE_CHAR4,
            GRP_SITE_CHAR5,
            GRP_SOIL_CHEM,
            GRP_SOIL_STOCK,
            GRP_SOIL_TEX,
            GRP_PFCURVE,
            GRP_WTD_1,
            GRP_SWC,
            GRP_SOIL_WRB_GROUP,
            GRP_SOIL_ORDER,
            GRP_SOIL_CLASSIFICATION,
            GRP_SOIL_SERIES,
            GRP_SOIL_DEPTH
        }

       /* public static async Task<string> ReadBlobTxt(string path, int? type ) //"icos/FA-Lso/fieldbook/"
        {
            string content = "";
            path = @"icos\FA-Lso\fieldbook\" + path;
            var storageCredentials = new StorageCredentials("icos", "KnyTe3J/vek3L53ygGPLhGbJvffagd5iOinY/lmRvA/VDMBfZ3d1yOIkUwY+cR/UxlZtA9nwGIqp/K5siBN+2A==");
            CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
            CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient(); //.CreateCloudBlobClient();
            CloudFileShare fileShare = fileClient.GetShareReference("icosshare");
            await fileShare.CreateIfNotExistsAsync();
            CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(path);
            if (type == 15)
            { 
                using (StreamReader reader = new StreamReader(await cloudFile.OpenReadAsync()))
                {
                    content = reader.ReadToEnd();
                }
            }
            else if (type == 22)
            {
               // FileSharePermissions permissions = new FileSharePermissions();

                //await fileShare.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                content = "<img alt='' src='" + cloudFile.StorageUri.PrimaryUri.ToString() + "'>";
            }
            return content;
        }*/

    }
}
