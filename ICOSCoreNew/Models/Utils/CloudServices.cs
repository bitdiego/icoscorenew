﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.File;
using ICOSCoreNew.Models.CloudServices;

namespace ICOSCoreNew.Models.Utils
{
    public class CloudServices : ICloudServices
    {
        private readonly IConfiguration configuration;

        public CloudServices(IConfiguration _config)
        {
            configuration = _config;
        }

        public async Task<string> DownloadCloudFile(string siteFolder, int fileId, string fileName)
        {
            //string path =model.site_code;
            var storageCredentials = new StorageCredentials(configuration.GetSection("AzureFileShare").GetValue<string>("user"),
                                                                configuration.GetSection("AzureFileShare").GetValue<string>("password"));
            CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
            CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient();
            CloudFileShare fileShare = fileClient.GetShareReference(configuration.GetSection("AzureFileShare").GetValue<string>("shareName"));
            await fileShare.CreateIfNotExistsAsync();
            CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(siteFolder);

            try
            {
                if (await fileShare.ExistsAsync())
                {
                    CloudFileDirectory rootDir = fileShare.GetRootDirectoryReference();
                    CloudFileDirectory icosDir = rootDir.GetDirectoryReference(configuration.GetSection("AzureFileShare").GetValue<string>("root"));
                    if (await icosDir.ExistsAsync())
                    {
                        CloudFileDirectory siteDir = icosDir.GetDirectoryReference(siteFolder);
                        if (await siteDir.ExistsAsync())
                        {
                            cloudFile = siteDir.GetFileReference(fileName);
                            if (await cloudFile.ExistsAsync())
                            {
                                try
                                {
                                    var policy = new SharedAccessFilePolicy
                                    {
                                        Permissions = SharedAccessFilePermissions.Read,
                                        SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
                                        SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1)
                                    };
                                    var url = cloudFile.Uri.AbsoluteUri + cloudFile.GetSharedAccessSignature(policy);

                                    return (url);

                                }
                                catch (Exception e)
                                {
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
            return null;
        }

        public async Task<string> ReadBlobTxt(string path, int? type) //"icos/FA-Lso/fieldbook/"
        {
            string content = "";
            path = @"icos\FA-Lso\fieldbook\" + path;
            var storageCredentials = new StorageCredentials("icos", "KnyTe3J/vek3L53ygGPLhGbJvffagd5iOinY/lmRvA/VDMBfZ3d1yOIkUwY+cR/UxlZtA9nwGIqp/K5siBN+2A==");
            CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
            CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient(); //.CreateCloudBlobClient();
            CloudFileShare fileShare = fileClient.GetShareReference("icosshare");
            await fileShare.CreateIfNotExistsAsync();
            CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(path);
            if (type == 15)
            {
                using (StreamReader reader = new StreamReader(await cloudFile.OpenReadAsync()))
                {
                    content = reader.ReadToEnd();
                }
            }
            else if (type == 22)
            {
                // FileSharePermissions permissions = new FileSharePermissions();

                //await fileShare.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                content = "<img alt='' src='" + cloudFile.StorageUri.PrimaryUri.ToString() + "'>";
            }
            return content;
        }
    }
}
