﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.Utils
{
    public class CustomDateValidator : ValidationAttribute
    {

        public string MyMessage { get; set; }

        protected override ValidationResult
               IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }
            string ritorno = validateIsoDate(value.ToString());
            if (ritorno.Length == 0)
            {
                return ValidationResult.Success;
            }
            else
            {
                MyMessage = FormatErrorMessage(ritorno);
                return new ValidationResult
                    (MyMessage);
            }
        }

        private string validateIsoDate(string input)
        {
            //var input = $.trim(args.Value);
            //var d = new DateTime();

            DateTime d = DateTime.Today;
            var currYear = d.Year;

            int[] daysInMonth = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

          
            if ((input.Length) % 2 > 0 || input.Length > 12)
            {

                return " * Wrong IsoDate input: please enter yyyy[mm[dd][HH][MM]] ([]: optional fields)";

            }



            if (!System.Text.RegularExpressions.Regex.IsMatch(input, "^[0-9]+$"))
            {

                return " * Wrong IsoDate input: only digits allowed";

            }
            else
            {
                var subYear = input.Substring(0, 4);
                var subMonth = "";
                var subDay = "";
                var subHour = "";
                var subMins = "";
                ////////////////
                var iYear = 0;
                iYear = Convert.ToInt16(subYear);

                var iMonth = 0;
                var iDay = 0;
                var iHour = 0;
                var iMins = 0;
                if (iYear < 1850 || iYear > currYear)
                {

                    return " * Wrong YEAR in IsoDate input: must be between 1850 AND " + currYear.ToString();

                }
                if (input.Length > 4)
                {
                    subMonth = input.Substring(4, 2);
                    iMonth = Convert.ToInt16(subMonth);
                    if (iMonth < 1 || iMonth > 12)
                    {

                        return " * Wrong MONTH in IsoDate input: must be between 1 AND 12";

                    }
                }
                if (input.Length > 6)
                {
                    subDay = input.Substring(6, 2);
                    iDay = Convert.ToInt16(subDay);
                    if (iMonth != 2)
                    {

                        if (iDay > daysInMonth[iMonth - 1])
                        {

                            return " * Wrong DAYS in IsoDate input: must be <= " + daysInMonth[iMonth - 1].ToString();

                        }

                    }
                    else
                    {
                        if (isLeap(iYear))
                        {
                            if (iDay > 29)
                            {

                                return " * Wrong DAYS in IsoDate input: must be <= 29";

                            }
                        }
                        else
                        {
                            if (iDay > 28)
                            {

                                return " * Wrong DAYS in IsoDate input: must be <= 28";

                            }
                        }
                    }
                }
                if (input.Length > 8)
                {
                    subHour = input.Substring(8, 2);
                    iHour = Convert.ToInt16(subHour);
                    if (iHour >= 24)
                    {
                        return " * Wrong HOUR in IsoDate input: must be between 0 AND 23";
                    }
                }
                if (input.Length > 10)
                {
                    subMins = input.Substring(10, 2);
                    iMins = Convert.ToInt16(subMins);
                    if (iMins > 59)
                    {

                        return " * Wrong MINUTES in IsoDate input: must be between 0 AND 59";

                    }
                }

            }
            return "";
        }


        private bool isLeap(int yy)
        {
            if ((yy % 400 == 0 || yy % 100 != 0) && (yy % 4 == 0))
            {
                return true;

            }
            else
            {
                return false;
            }

        }

    }
}
