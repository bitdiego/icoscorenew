﻿using ICOSCoreNew.DB;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.ViewModel;
//using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICOSCoreNew.Models.CloudServices;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ICOSCoreNew.Models.DataServices
{
    public class EFDataServices : IDataStorageServices
    {
        private readonly ICOSEntities dbContext;
        private readonly ICloudServices iCloudServices;

        public EFDataServices(ICOSEntities dbContext, ICloudServices cloudService)
        {
            this.dbContext = dbContext;
            this.iCloudServices = cloudService;
        }

        //public async Task<List<ICOS_site>> GetIcosSitesAsync()
        public async Task<List<SiteViewModel>> GetIcosSitesAsync()
        {
            try
            {
                List<SiteViewModel> sites = await dbContext.vIcos_site.ToListAsync();
                return sites;
            }
            catch(Exception e)
            {
                return null;
            }
            
        }

        public async Task<List<IcosSitePI>> GetPISitesListAsync()
        {
            try
            {
                List<IcosSitePI> icosSitesPiList = await dbContext.IcosSitePIs.Where(item => item.id_users == AccountDA.CurrentUserID).OrderBy(item => item.site_code).ToListAsync(); //new List<IcosSitePI>();
                //AccountDA.CurrentUserID

                return icosSitesPiList;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<ICOSSitePiViewModel> GetPISitesViewModelListAsync()
        {
            List<IcosSitePI> _tList = await GetPISitesListAsync();
            ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
            viewModel.sitesPiList = new List<SelectListItem>();

            foreach (var _t in _tList)
            {
                viewModel.sitesPiList.Add(new SelectListItem(_t.site_code, _t.id_site.ToString()));
            }

            return viewModel;
        }

        public int GetSiteIdBySiteCode(string siteCode)
        {
            try
            { 
                var _site = dbContext.ICOS_site.FirstOrDefault(site =>  String.Compare( site.site_code,siteCode, true)==0);
                return _site.id_icos_site;
            }
            catch(Exception e)
            {
                throw new ArgumentNullException();
            }
        }

        public void GetSiteIdClassBySiteCode(string siteCode, ref int _id, ref short? _class)
        {
            try
            {
                var _site = dbContext.ICOS_site.FirstOrDefault(site => String.Compare(site.site_code, siteCode, true) == 0);
                _id = _site.id_icos_site;
                _class = _site.@class;
            }
            catch(Exception e)
            {
                throw new ArgumentNullException();
            }
        }


        public async Task<bool> CPRecordsExistAsync(int groupId, int siteID, string value)
        {
            int cp = await dbContext.DataStorages.Where(ds => ds.siteID == siteID && ds.groupID == (int)Globals.BadmVarsGroups.GRP_PLOT 
                                                        && ds.qual0.IndexOf(value) >= 0 && ds.dataStatus == 0 && ds.groupName=="" ).CountAsync();
            return cp > 0;
        }

        public async Task<bool> SaveFileInDbAsync(string fileName, string newFileName, int siteId, int fileType, int piSession)
        {
            try
            { 
                icosfile icFile = new icosfile();
                icFile.originalname = fileName;
                icFile.site = siteId;
                icFile.data_type = fileType;
                icFile.date = DateTime.Now;
                icFile.status = "Uploaded";
                icFile.description = "Uploaded";
                icFile.idins = piSession;
                icFile.nome = newFileName;
                dbContext.icosfiles.Add(icFile);
                await dbContext.SaveChangesAsync();
                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public async Task<bool> SaveFileInFieldbookAsync(string fileName, string dataInfo, int _siteId, int dataType, int piSession)
        {
            try
            {
                Fieldbook fb = new Fieldbook()
                {
                    idins = piSession,
                    originalname = fileName,
                    siteId = _siteId,
                    data_info = dataInfo,
                    date = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    importDate = DateTime.Now,
                    status = 0,
                    data_type =dataType
                 };
                dbContext.Fieldbooks.Add(fb);
                await dbContext.SaveChangesAsync();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }

        }
        
        public async Task<List<Fieldbook>> GetFieldbookItems(int siteId)
        {
            List<Fieldbook> fbList = new List<Fieldbook>();
            fbList = await dbContext.Fieldbooks.Where(items => items.siteId == siteId).ToListAsync();
            return fbList;
        }

        public async Task<List<FieldBookViewModel>> GetFieldBookVMItems(int siteId)
        {
            List<Fieldbook> fbList = new List<Fieldbook>();
            List<FieldBookViewModel> fbListVM = new List<FieldBookViewModel>();
            fbList = await dbContext.Fieldbooks.Where(items => items.siteId == siteId).ToListAsync();
            foreach (var fb in fbList)
            {
                fbListVM.Add(new FieldBookViewModel()
                {
                    OriginalName=fb.originalname,
                    DataInfo=fb.data_info,
                    Content = await iCloudServices.ReadBlobTxt(fb.originalname, fb.data_type),
                    ///////if image-> 
                    ImportDate=fb.importDate, 
                    SubmitterName = dbContext.Users.FirstOrDefault(user => user.id_users == fb.idins).Name
                });
            }
            return fbListVM;
        }

        public async Task<List<SubmittedFilesViewModel>> GetSubmittedFilesAsync(int siteId)
        {
            List<icosfile> iFiles = await dbContext.icosfiles.Where(file => file.site == siteId).ToListAsync();
            List<SubmittedFilesViewModel> subFiles = new List<SubmittedFilesViewModel>();
            foreach(var file in iFiles)
            {
                subFiles.Add(new SubmittedFilesViewModel()
                {
                    Name=file.nome,
                    OriginalName=file.originalname,
                    FileType=dbContext.FileTypes.FirstOrDefault(ff => ff.id_filetype==file.data_type).type,
                    SumissionDate=file.date,
                    Submitter = dbContext.Users.FirstOrDefault(user => user.id_users == file.idins).Name,
                    Description = file.description,
                    Status=file.status,
                    Link = file.data_type != 20 ? "<button onclick='downloadSubmittedFile(" + file.ID + ");'><i class='fa fa-cloud-download'  title='Download'></i></button>" : "Carbon Portal"
                });
            }

            return subFiles;
        }

        public async Task<string> DownloadSubmittedFile(/*int siteId, */int fileId)
        {
            var ff = await dbContext.icosfiles.Where(file => file.ID == fileId /*&& file.site == siteId*/).SingleOrDefaultAsync();
            var siteFolder = dbContext.ICOS_site.SingleOrDefault(site => site.id_icos_site == ff.site).site_code;
            if (ff!=null)
            {
                return await iCloudServices.DownloadCloudFile(siteFolder, fileId, ff.nome);
            }
            return "";
        }

        /*public async Task<List<IcosSitePI>> GetPISitesListAsync()
        {
            List<IcosSitePI> icosSitesPiList = new List<IcosSitePI>();
            icosSitesPiList = await dbContext.IcosSitePIs.Where(v => v.id_users == AccountDA.CurrentUserID).OrderBy(s => s.site_code).ToListAsync();
            return icosSitesPiList;
        }*/


    }
}
