﻿using ICOSCoreNew.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.SqlDbServices
{
    public interface IUserLoginServices
    {
        UserModel Login(LoginModel model);
        void ChangePassword(ChangePasswordModel model);
        ResetPasswordModel ResetPassword(ResetPasswordModel model);
        Task<UserModel> CheckLoginAsync(LoginModel model);
    }
}
