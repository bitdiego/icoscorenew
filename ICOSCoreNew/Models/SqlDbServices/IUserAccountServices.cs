﻿using ICOSCoreNew.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.SqlDbServices
{
    public interface IUserAccountServices
    {
        Task<UserModel> CheckLoginAsync();
    }
}
