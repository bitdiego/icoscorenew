﻿using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.SqlDbServices
{
    public interface IDataStorageServices
    {
        Task<List<SiteViewModel>> GetIcosSitesAsync();
        Task<List<IcosSitePI>> GetPISitesListAsync();
        Task<ICOSSitePiViewModel> GetPISitesViewModelListAsync();
        int GetSiteIdBySiteCode(string siteCode);
        void GetSiteIdClassBySiteCode(string siteCode, ref int _id, ref short? _class);
        Task<bool> CPRecordsExistAsync(int groupId, int siteID, string value);
        Task<bool> SaveFileInDbAsync(string fileName, string newFileName, int siteId, int fileType, int piSession);
        Task<bool> SaveFileInFieldbookAsync(string fileName, string dataInfo, int _siteId, int dataType, int piSession);
        Task<List<Fieldbook>> GetFieldbookItems(int siteId);
        Task<List<FieldBookViewModel>> GetFieldBookVMItems(int siteId);
        Task<List<SubmittedFilesViewModel>> GetSubmittedFilesAsync(int siteId);
        Task<string> DownloadSubmittedFile(/*int siteId,*/ int fileId);
        //int GetSiteClassBySiteCode(string siteCode);


/*
 * 
*/
}
}
