﻿using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.SqlDbServices
{
    public interface ISitesListService
    {
        IEnumerable<IcosSitePI> GetPISitesList();
    }
}
