﻿using System.Threading.Tasks;

namespace ICOSCoreNew.Models.CloudServices
{
    public interface ICloudServices
    {
        Task<string> DownloadCloudFile(string siteFolder, int fileId, string fileName);
        Task<string> ReadBlobTxt(string path, int? type);
    }
}
