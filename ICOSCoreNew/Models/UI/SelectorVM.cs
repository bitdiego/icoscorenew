﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ICOSCoreNew.Models.UI
{
    public class SelectorVM
    {
        [Key]
        public int Id_site { get; set; }

        [NotMapped]
        [Display(Name = "Sites", Prompt = "Sites", Description = "Pick a site")]
        public SelectList SiteList { get; set; }
    }
}