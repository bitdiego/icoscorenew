﻿using ICOSCoreNew.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ICOSCoreNew.Models.UI
{
    public class LocationGroupUI
    {
       // private IEnumerable<SelectListItem> _allcameratypes;
      // private IEnumerable<SelectListItem> _allcameralenstypes;
        public LocationGroupUI()
        {

        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required field")]
        [RegularExpression(@"^[+-]?((\d+(\.\d*)?)|(\.\d+))$", ErrorMessage = "Please enter a decimal number")]
        [Remote("CheckSPLatitude", "LocationGroup", HttpMethod = "POST", ErrorMessage = "Error: LONGITUDE must be between -90 and 90 degrees")]
        public string LOCATION_LAT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Required field")]
        [RegularExpression(@"^[+-]?((\d+(\.\d*)?)|(\.\d+))$", ErrorMessage = "Please enter a decimal number")]
        [Remote("CheckSPLongitude", "LocationGroup", HttpMethod = "POST", ErrorMessage = "Error: LONGITUDE must be between -180 and 180 degrees")]
        public string LOCATION_LONG { get; set; }

        public string LOCATION_ELEV { get; set; }
        [CustomDateValidator(ErrorMessage = "Date format error")]
        public string LOCATION_DATE { get; set; }

        [MaxLength(512, ErrorMessage ="Max number of characters (512) exceeded")]
        public string LOCATION_COMMENT { get; set; }
    }
}
