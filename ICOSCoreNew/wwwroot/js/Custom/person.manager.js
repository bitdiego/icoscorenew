﻿var personMgr = (function () {
    let gridid = "";
    let dialogid = "";
    let actionsave = "";
    let actionupdate = "";
    let actiondelete = "";
    let actionloaddata = "";
    let actiondlg = "";
    
    function personMgr(gridId, idDialog, actionSave, actionUpdate, actionDelete, actionLoadData, actionDlg) {
        gridid = gridId;
        dialogid = idDialog;
        actionsave = actionSave;
        actionupdate = actionUpdate;
        actiondelete = actionDelete;
        actionloaddata = actionLoadData;
        actiondlg = actionDlg;
    }
    personMgr.prototype.Initialize = function () {
        Init();
    };
    personMgr.prototype.confirmCallBack = function (indata) {
        let action = "";
        //var rowTemplateHTML="<tr><td data-row-info=\"Id\">{0}</td><td data-row-info=\"dataRiferimento\">{1}</td><td data-row-info=\"idCommessa\">{2}</td><td data-row-info=\"festivo\"><input type=\"\" disabled=\"disabled\">{3}</input></td><td data-row-info=\"ore\">{4}</td><td><a class=\"pointer\" data-row-info=\"linkUpdate\"><i class=\"fa fa-pencil-square\" aria-hidden=\"true\">Modifica</i></a></td><td><a class=\"pointer\" data-row-info=\"linkDelete\" role=\"button\"><i class=\"fa fa-times-circle\" aria-hidden=\"true\">Cancella</i></a></td></tr>";
        let rowTemplateHTML = "<tr><td data-row-info=\"Id\">{0}</td><td data-row-info=\"dataRiferimento\">{1}</td><td data-row-info=\"idCommessa\">{2}</td><td data-row-info=\"festivo\"><input disabled=\"disabled\" class=\"check-box\" type=\"checkbox\" {3}></td><td data-row-info=\"ore\">{4}</td><td><a class=\"pointer\" data-row-info=\"linkUpdate\"><i class=\"fa fa-pencil-square\" aria-hidden=\"true\">Modifica</i></a></td><td><a class=\"pointer\" data-row-info=\"linkDelete\" role=\"button\"><i class=\"fa fa-times-circle\" aria-hidden=\"true\">Cancella</i></a></td></tr>";
        let inputdata = indata.data === undefined ? indata : indata.data.oreGiornata;
        let currentactionreq = actionsave;
        //debugger;
        //in questo caso devo aggiungere una riga alla fine della tabella
        $.ajax({
            url: currentactionreq,
            dataType: 'json',
            //contentType: 'application/json',
            data: inputdata,
            method: "POST",
            timeout: 0
        })
            .done(function (result, textStatus) {
                if (result.ReturnValue === 0) {
                    $(gridid).refresh();
                    //var re=/-?\d+/;
                    //var m=re.exec(result.ReturnData.dataRiferimento);
                    //var d=new Date(parseInt(m[0]));
                    //let insert=true;
                    ////ricerco la riga in lavorazione
                    //$("#tblStraordinari tbody tr td[data-row-info='Id']").each(function() {
                    //  if($(this).text()==result.ReturnData.Id) {
                    //    insert=false;
                    //    //faccio la replace se sono in update
                    //    if(-1!=currentactionreq.indexOf(actionupdate)) {
                    //      $(this).parent().replaceWith(format(rowTemplateHTML,[result.ReturnData.Id,d.toLocaleDateString(),result.ReturnData.IdCommessa,result.ReturnData.Festivo?"checked='checked'":"",result.ReturnData.Ore]));
                    //    }else if(-1!=currentactionreq.indexOf(actiondelete)) {
                    //      $(this).parent().remove();
                    //    }
                    //    return false;
                    //  }
                    //});
                    ////aggiungo la nuova riga se sono in inserimento
                    //if(insert) {
                    //  $("#tblStraordinari tbody").append(format(rowTemplateHTML,[result.ReturnData.Id,d.toLocaleDateString(),result.ReturnData.IdCommessa,result.ReturnData.Festivo?"checked='checked'":"",result.ReturnData.Ore]));
                    //}
                } else {
                    //non è possibile e mostra l'errore
                    alert(result.Message);
                }

            })
            .fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
    };

    function BindEvents() {
        $("#tblStraordinari #btnAddNew").on('click', '', function () {
            var tipologiaOraDlg = new tipologiaOraDialog(dialogid);
            tipologiaOraDlg.initializeDialog(personMgr.prototype.confirmCallBack, actiongettipologia, actionsave, actiondlg);
            tipologiaOraDlg.show();
        });
    }
    function fillDatatable() {
        $(gridid).jsGrid({
            width:"100%",
            height: "auto",
            filtering: true,
            editing: true,
            inserting: false,
            sorting: true,
            paging: true,
            pageSize: 50,
            autoload: true,
            controller: {
                loadData: function (filter) {
                    var d = $.Deferred();
                    $.ajax({
                        url: actionloaddata,
                        type: "POST",
                        data: filter,
                        //Output
                        dataType: "json"
                    }).done(function (response) {
                        d.resolve(response);
                    });

                    return d.promise();
                },
                deleteItem: function (args) {
                    var f = $.Deferred();
                    $.ajax({
                        url: actiondelete,
                        type: "POST",
                        data: args,
                        //output
                        dataType: "json"
                    }).done(function (response) {
                        $(gridid).jsGrid("loadData");
                    });
                    return f.promise();
                },
                editItem: function (args) {
                    alert(args);
                }
            },
            onItemEditing: function (args) {
                // cancel editing of the row of item with field 'ID' = 0
                args.cancel = true;
                showDetailsDialog("Edit", args.item);
            },
            rowClick: function (args) {
                if (args.event.target) {

                }
                args.cancel = true;
                //showDetailsDialog("Edit", args.item);
            },
            rowClass: function (item, itemIndex) {
                var $result = $([]);
                if (2 === item.dataStatus) {
                    return "jsgridreadOnlyRow";
                }
            },
            fields: [
                { name: "TEAM_MEMBER_NAME", type: "text", title: "TEAM MEMBER NAME", width: "60px" },
                { name: "TEAM_MEMBER_ROLE_DESCRIPTION", type: "text", title: "TEAM MEMBER ROLE", width: "120px" },
                { name: "TEAM_MEMBER_MAIN_EXPERT_DESCRIPTION", type: "text", title: "TEAM MEMBER MAIN EXPERT", width: "160px" },
                { name: "TEAM_MEMBER_PERC_ICOS", type: "text", title: "TEAM MEMBER PERC ICOS", width: "150px", filtering: false },
                { name: "TEAM_MEMBER_CONTRACT_DESCRIPTION", type: "text", width: "160px" },
                { name: "TEAM_MEMBER_DATE", type: "text", width: "120px" },
                //{name: "ranger",type:"NumberRange"},
                {
                    type: "control",
                    //editButton: true,
                    headerTemplate: function () {
                        return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                                .on("click", function () {
                                    showDetailsDialog("Add", null);
                                });
                    },
                    itemTemplate: function (value, item) {
                        //debugger;
                        var $result = $([]);
                        $result = $result.add(this._createEditButton(item));
                        if (item.Editable) {
                        }
                        $result = $result.add(this._createDeleteButton(item));
                        if (item.Deletable) {

                        }
                        return $result;
                    }
                }
            ],
        });
    }
    function showDetailsDialog(action, data) {
        //debugger;
        var persondialog = new personDialog(dialogid, action);
        persondialog.initializeDialog(personMgr.prototype.confirmCallBack, actiondlg, action, data);
        persondialog.show();
    }
    function Init() {
        fillDatatable();
    }
    function format(str, args) {
        var internalstr = str;
        for (i = 0; i < args.length; i++)
            internalstr = internalstr.replace("{" + i + "}", args[i]);
        return internalstr;
    }
    return personMgr;
}());
