﻿var dialogMgr=(function() {
  let dialogid="";
  let actionRetrieveDialog="";
  //let currentActionReq=0;
  let delegatebtnSave;
  let dataInput=null;
  let datepickerObj = null;
  let linkeddatepickers = null;
  function dialogMgr(iddialog,dialogTitle) {
    dialogid=iddialog;
    if(undefined!==dialogTitle && null!==dialogTitle)$(dialogid+" #title").text(dialogTitle);
  }
  dialogMgr.prototype.initializeDialog = function (delegatebtnsave, actionretrievedialog,data,linkedpickers) {
      delegatebtnSave = delegatebtnsave;
      actionRetrieveDialog = actionretrievedialog;
      //currentActionReq=currentactionreq;
      dataInput=data;
      linkeddatepickers = linkedpickers;
      Init();
  };
  dialogMgr.prototype.show = function () {
      $(dialogid).modal('show');
  };
  dialogMgr.prototype.hide= function () {
      $(dialogid).modal('hide');
  };
  //private methods
  function BindEvents() {
    //Events
    //$(dialogid+" #btnsave").on('click',function(e) {
    //if ($("form").valid()) {
    //    values=serializeToArray('formInput');
    //    delegatebtnSave(values,currentActionReq);
    //    //$(dialogid+" #btnsave").off('click');
    //  }else{
    //    return false;
    //  }
    //});
    //$("a .linkUpdate").on('click',function() {
    //  var itemId=$(this).attr('data-itemid');
    //  //alert("Il valore dell'id è ".concat(itemId));
    //});
    $("#formInput").on("submit", function (e) {
        var isvalid = $("#formInput").valid();
        if (isvalid) {
            //$(".validation-summary-errors ul:first-of-type").empty();
            $.post($(this).attr("action"),
                    $(this).serialize(),
                    function (data) {
                        if (0 === data.code) {
                            $("#dlgStruct").modal("hide");
                            //
                            $("#jsGrid").jsGrid("render");
                        } else {
                            var element=$(".validation-summary-valid.text-danger");
                            if (element.length==0) {
                                element = $(".text-danger.validation-summary-errors")
                            } else {
                                element.attr('class', 'validation-summary-errors text-danger');
                            }
                            element.children("ul:first-of-type").empty().append("<li>" + data.message + "</li>");
                        }
                    });
            return false;
        }
        e.preventDefault();
    });
    datepickerObj = new datepickerMgr();
    datepickerObj.InitiliazeDatePickers(true);
    if (undefined !== linkeddatepickers && null !== linkeddatepickers) {
        datepickerObj.linkDatePickers(linkeddatepickers);
    }
    //events
  }
  function Init() {
    //Controls
    $.ajax({
      url: actionRetrieveDialog,
      data:dataInput,
      cache:false,
      type: null === dataInput ? "GET" : "POST",
      beforeSend: function () {
          //imposta il loader
          $("#bodyLoader").show();
        },
    }).done(function (data) {
        $("#dlgStruct div[class='modal-body']").html(data);
        BindEvents();
    })
    .fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    })
      .always(function() {
          //devo nascondere il loader...
          $("#bodyLoader").hide();
      });
  }
  function serializeToArray(idElement) {
    var result;
    result=JSON.parse(JSON.stringify($("#"+idElement).find(':not(:checkbox)').serializeArray()));
    $("#"+idElement+" input:checkbox").each(function() {
      result.push({ name: this.name,value: this.checked });
    });
    var formData = $(this).serialize();
    return result;
  }
  return dialogMgr;
}());