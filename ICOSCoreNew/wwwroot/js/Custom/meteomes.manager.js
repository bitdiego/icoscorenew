﻿var meteoMeasMgr=(function() {
  let gridid="";
  let dialogid="";
  let actionsave="";
  let actionupdate="";
  let actiondelete="";
  let actionloaddata="";
  let actiondlg="";

  function meteoMeasMgr(gridId,idDialog,actionSave,actionUpdate,actionDelete,actionLoadData,actionDlg) {
    gridid=gridId;
    dialogid=idDialog;
    actionsave=actionSave;
    actionupdate=actionUpdate;
    actiondelete=actionDelete;
    actionloaddata=actionLoadData;
    actiondlg=actionDlg;
  }
  meteoMeasMgr.prototype.Initialize=function() {
    Init();
  };
  meteoMeasMgr.prototype.confirmCallBack=function(indata) {
    let action="";
    let inputdata=indata.data===undefined?indata:indata.data.oreGiornata;
    let currentactionreq=actionsave;
    //in questo caso devo aggiungere una riga alla fine della tabella
    $.ajax({
      url: currentactionreq,
      dataType: 'json',
      //contentType: 'application/json',
      data: inputdata,
      method: "POST",
      timeout: 0
    })
    .done(function(result,textStatus) {
      if(result.ReturnValue===0) {
        $(gridid).refresh();
      } else {
        alert(result.Message);
      }

    })
    .fail(function(jqXHR,textStatus) {
      alert("Request failed: "+textStatus);
    });
  };

  function BindEvents() {
  }
  function fillDatatable() {
    $(gridid).jsGrid({
      width:"100%",
      height: "auto",
      filtering: true,
      editing: true,
      inserting: false,
      sorting: true,
      paging: true,
      pageSize: 20,
      autoload: true,
      controller: {
        loadData: function(filter) {
          var d=$.Deferred();
          $.ajax({
            url: actionloaddata,
            type: "POST",
            data: filter,
            //Output
            dataType: "json"
          }).done(function(response) {
            d.resolve(response);
          });

          return d.promise();
        },
        deleteItem: function(args) {
          var f=$.Deferred();
          $.ajax({
            url: actiondelete,
            type: "POST",
            data: args,
            //output
            dataType: "json"
          }).done(function(response) {
            $(gridid).jsGrid("loadData");
          });
          return f.promise();
        },
        editItem: function(args) {
          alert(args);
        }
      },
      onItemEditing: function(args) {
        // cancel editing of the row of item with field 'ID' = 0
        args.cancel=true;
        showDetailsDialog("Edit Meteo Measurement", args.item);
      },
      rowClick: function(args) {
        args.cancel=true;
      },
      rowClass: function(item,itemIndex) {
        var $result=$([]);
        if(2===item.dataStatus) {
          return "jsgridreadOnlyRow";
        }
      },
      fields: [
          { name: "BM_MODEL",type: "text",title: "BM_MODEL",width: "120px" },
          { name: "BM_SN",type: "text",title: "BM_SN",width: "120px" },
          { name: "BM_TYPE_DESCRIPTION",type: "text",title: "BM_TYPE",width: "120px" },
          { name: "BM_VARIABLE_H_V_R_DESCRIPTION",type: "text",title: "BM_VARIABLE_H_V_R",width: "120px" },
          { name: "BM_TYPE_DESCRIPTION",type: "text",title: "BM_TYPE",width: "120px" },
          { name: "BM_HEIGHT",type: "text",title: "BM_HEIGHT",width: "120px" },
          { name: "BM_EASTWARD_DIST",type: "text",title: "BM_EASTWARD_DIST",width: "120px" },
          {
            type: "control",
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                      .on("click",function() {
                          showDetailsDialog("Add Meteo Measurement", null);
                      });
            },
            itemTemplate: function(value,item) {
                var $result = $([]);
                if (item.dataStatus == 0) {
                    $result = $result.add(this._createEditButton(item));
                    $result = $result.add(this._createDeleteButton(item));
                }              
                return $result;
            }
          }
      ],
    });
  }
  function showDetailsDialog(action,data) {
    var dialogmgr=new dialogMgr(dialogid,action);
    dialogmgr.initializeDialog(meteoMeasMgr.prototype.confirmCallBack,actiondlg,data);
   // dialogmgr.linkDatePickers('BM_DATE_START','BM_DATE_END');
    dialogmgr.show();
  }
  function Init() {
    fillDatatable();
  }
  function format(str,args) {
    var internalstr=str;
    for(i=0;i<args.length;i++)
      internalstr=internalstr.replace("{"+i+"}",args[i]);
    return internalstr;
  }
  return meteoMeasMgr;
}());
