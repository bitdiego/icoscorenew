﻿var storageGroupMgr=(function() {
  let gridid="";
  let dialogid="";
  let actionsave="";
  let actionupdate="";
  let actiondelete="";
  let actionloaddata="";
  let actiondlg="";

  function storageGroupMgr(gridId,idDialog,actionSave,actionUpdate,actionDelete,actionLoadData,actionDlg) {
    gridid=gridId;
    dialogid=idDialog;
    actionsave=actionSave;
    actionupdate=actionUpdate;
    actiondelete=actionDelete;
    actionloaddata=actionLoadData;
    actiondlg=actionDlg;
  }
  storageGroupMgr.prototype.Initialize=function() {
    Init();
  };
  storageGroupMgr.prototype.confirmCallBack=function(indata) {
    let action="";
    let inputdata=indata.data===undefined?indata:indata.data.oreGiornata;
    let currentactionreq=actionsave;
    //in questo caso devo aggiungere una riga alla fine della tabella
    $.ajax({
      url: currentactionreq,
      dataType: 'json',
      //contentType: 'application/json',
      data: inputdata,
      method: "POST",
      timeout: 0
    })
    .done(function(result,textStatus) {
      if(result.ReturnValue===0) {
        $(gridid).refresh();
      } else {
        alert(result.Message);
      }

    })
    .fail(function(jqXHR,textStatus) {
      alert("Request failed: "+textStatus);
    });
  };

  function BindEvents() {
  }
  function fillDatatable() {
    $(gridid).jsGrid({
      width:"100%",
      height: "auto",
      filtering: true,
      editing: true,
      inserting: false,
      sorting: true,
      paging: true,
      pageSize: 20,
      autoload: true,
      controller: {
        loadData: function(filter) {
          var d=$.Deferred();
          $.ajax({
            url: actionloaddata,
            type: "POST",
            data: filter,
            //Output
            dataType: "json"
          }).done(function(response) {
            d.resolve(response);
          });

          return d.promise();
        },
        deleteItem: function(args) {
          var f=$.Deferred();
          $.ajax({
            url: actiondelete,
            type: "POST",
            data: args,
            //output
            dataType: "json"
          }).done(function(response) {
            $(gridid).jsGrid("loadData");
          });
          return f.promise();
        },
        editItem: function(args) {
          alert(args);
        }
      },
      onItemEditing: function(args) {
        // cancel editing of the row of item with field 'ID' = 0
        args.cancel=true;
        showDetailsDialog("Edit Storage Group", args.item);
      },
      rowClick: function(args) {
        args.cancel=true;
      },
      rowClass: function(item,itemIndex) {
        var $result=$([]);
        if(2===item.dataStatus) {
          return "jsgridreadOnlyRow";
        }
      },
      fields: [
          { name: "STO_CONFIG_DESCRIPTION",type: "text",title: "STO_CONFIG",width: "120px" },
          { name: "STO_VARIABLE_DESCRIPTION",type: "text",title: "STO_VARIABLE",width: "120px" },
          { name: "STO_BUFFER_VOL",type: "text",title: "STO_BUFFER_VOL",width: "120px" },
          { name: "STO_BUFFER_FLOW_RATE",type: "text",title: "STO_BUFFER_FLOW_RATE",width: "120px" },
          { name: "STO_TUBE_LENGTH",type: "text",title: "STO_TUBE_LENGTH",width: "120px" },
          { name: "STO_TUBE_DIAM",type: "text",title: "STO_TUBE_DIAM",width: "120px" },
          {
            type: "control",
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                      .on("click",function() {
                          showDetailsDialog("Add Storage Group", null);
                      });
            },
            itemTemplate: function(value,item) {
                var $result = $([]);
                if (item.dataStatus == 0) {
                    $result = $result.add(this._createEditButton(item));
                    $result = $result.add(this._createDeleteButton(item));
                }
                else {
                    $result = $result.add("<span class='fa-stack fa-lg'><i class='fa fa-pencil fa-stack-1x'></i><i class='fa fa-ban fa-stack-2x fa-rotate-90 text-danger'></i></span>");
                }
                return $result;
            }
          }
      ],
    });
  }
  function showDetailsDialog(action,data) {
    var dialogmgr = new dialogMgr(dialogid, action);
    dialogmgr.initializeDialog(storageGroupMgr.prototype.confirmCallBack,actiondlg,data,['STO_DATE_START', 'STO_DATE_END']);
    dialogmgr.show();
  }
  function Init() {
    fillDatatable();
  }
  function format(str,args) {
    var internalstr=str;
    for(i=0;i<args.length;i++)
      internalstr=internalstr.replace("{"+i+"}",args[i]);
    return internalstr;
  }
  return storageGroupMgr;
}());
