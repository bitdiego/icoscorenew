﻿var utcOffsetMgr=(function() {
  let gridid="";
  let dialogid="";
  let actionsave="";
  let actionupdate="";
  let actiondelete="";
  let actionloaddata="";
  let actiondlg="";

  function utcOffsetMgr(gridId,idDialog,actionSave,actionUpdate,actionDelete,actionLoadData,actionDlg) {
    gridid=gridId;
    dialogid=idDialog;
    actionsave=actionSave;
    actionupdate=actionUpdate;
    actiondelete=actionDelete;
    actionloaddata=actionLoadData;
    actiondlg=actionDlg;
  }
  utcOffsetMgr.prototype.Initialize=function() {
    Init();
  };
  utcOffsetMgr.prototype.confirmCallBack=function(indata) {
    let action="";
    //var rowTemplateHTML="<tr><td data-row-info=\"Id\">{0}</td><td data-row-info=\"dataRiferimento\">{1}</td><td data-row-info=\"idCommessa\">{2}</td><td data-row-info=\"festivo\"><input type=\"\" disabled=\"disabled\">{3}</input></td><td data-row-info=\"ore\">{4}</td><td><a class=\"pointer\" data-row-info=\"linkUpdate\"><i class=\"fa fa-pencil-square\" aria-hidden=\"true\">Modifica</i></a></td><td><a class=\"pointer\" data-row-info=\"linkDelete\" role=\"button\"><i class=\"fa fa-times-circle\" aria-hidden=\"true\">Cancella</i></a></td></tr>";
    let rowTemplateHTML="<tr><td data-row-info=\"Id\">{0}</td><td data-row-info=\"dataRiferimento\">{1}</td><td data-row-info=\"idCommessa\">{2}</td><td data-row-info=\"festivo\"><input disabled=\"disabled\" class=\"check-box\" type=\"checkbox\" {3}></td><td data-row-info=\"ore\">{4}</td><td><a class=\"pointer\" data-row-info=\"linkUpdate\"><i class=\"fa fa-pencil-square\" aria-hidden=\"true\">Modifica</i></a></td><td><a class=\"pointer\" data-row-info=\"linkDelete\" role=\"button\"><i class=\"fa fa-times-circle\" aria-hidden=\"true\">Cancella</i></a></td></tr>";
    let inputdata=indata.data===undefined?indata:indata.data.oreGiornata;
    let currentactionreq=actionsave;
    if(currentactionrequest==="Update") currentactionreq=actionupdate;
    $.ajax({
      url: currentactionreq,
      dataType: 'json',
      //contentType: 'application/json',
      data: inputdata,
      method: "POST",
      timeout: 0
    })
        .done(function(result,textStatus) {
          if(result.ReturnValue===0) {
            $(gridid).refresh();
          } else {
            alert(result.Message);
          }
        })
        .fail(function(jqXHR,textStatus) {
          alert("Request failed: "+textStatus);
        });
  };

  function BindEvents() {
  }
  function fillDatatable() {
    $(gridid).jsGrid({
      width:"100%",
      height: "auto",
      filtering: true,
      editing: true,
      inserting: false,
      sorting: true,
      paging: true,
      pageSize: 50,
      autoload: true,
      controller: {
        loadData: function(filter) {
          var d=$.Deferred();
          $.ajax({
            url: actionloaddata,
            type: "POST",
            data: filter,
            //Output
            dataType: "json"
          }).done(function(response) {
            d.resolve(response);
          });

          return d.promise();
        },
        deleteItem: function(args) {
          var f=$.Deferred();
          $.ajax({
            url: actiondelete,
            type: "POST",
            data: args,
            //output
            dataType: "json"
          }).done(function(response) {
            $(gridid).jsGrid("loadData");
          });
          return f.promise();
        },
        editItem: function(args) {
          alert(args);
        }
      },
      onItemEditing: function(args) {
        // cancel editing of the row of item with field 'ID' = 0
        args.cancel=true;
        showDetailsDialog("Edit UTC Offset",args.item);
      },
      rowClick: function(args) {
        args.cancel=true;
      },
      rowClass: function(item,itemIndex) {
        var $result=$([]);
        if(2===item.dataStatus) {
          return "jsgridreadOnlyRow";
        }
      },
      fields: [
          { name: "UTC_OFFSET",type: "text",title: "UTC OFFSET",width: "60px" },
          { name: "UTC_DATE_START",type: "text",title: "UTC DATE START",width: "60px" },
          { name: "UTC_COMMENT",type: "text",title: "UTC COMMENT",width: "60px" },
          {
            type: "control",
            //editButton: true,
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                  .on("click",function() {
                    showDetailsDialog("Add UTC Offset",null);
                  });
            },
            itemTemplate: function(value,item) {
                var $result = $([]);
                if (item.dataStatus == 0) {
                    $result = $result.add(this._createEditButton(item));
                    $result = $result.add(this._createDeleteButton(item));
                }              
                return $result;
            }
          }
      ],
    });
  }
  function showDetailsDialog(action,data) {
    var dialogmgr = new dialogMgr(dialogid, action);
    dialogmgr.initializeDialog(utcOffsetMgr.prototype.confirmCallBack,actiondlg,data);
    dialogmgr.show();
  }
  function Init() {
    fillDatatable();
  }
  function format(str,args) {
    var internalstr=str;
    for(i=0;i<args.length;i++)
      internalstr=internalstr.replace("{"+i+"}",args[i]);
    return internalstr;
  }
  return utcOffsetMgr;
}());
