﻿var foliarSamplingMgr = (function () {
    let gridid = "";
    let dialogid = "";
    let actionsave = "";
    let actionupdate = "";
    let actiondelete = "";
    let actionloaddata = "";
    let actiondlg = "";
    let dialogmgr = null;
    function foliarSamplingMgr(gridId, idDialog, actionSave, actionUpdate, actionDelete, actionLoadData, actionDlg) {
        gridid = gridId;
        dialogid = idDialog;
        actionsave = actionSave;
        actionupdate = actionUpdate;
        actiondelete = actionDelete;
        actionloaddata = actionLoadData;
        actiondlg = actionDlg;
    }
    foliarSamplingMgr.prototype.Initialize = function () {
        Init();
    };
    foliarSamplingMgr.prototype.confirmCallBack = function (indata, currentactionrequest) {
        let currentactionreq = actionsave;
        if (currentactionrequest === "Edit") currentactionreq = actionupdate;
        //in questo caso devo aggiungere una riga alla fine della tabella
        $.ajax({
            url: currentactionreq,
            dataType: 'json',
            //contentType: 'application/json',
            data: indata,
            method: "POST",
            timeout: 0
        })
        .done(function (result, textStatus) {
            if (result.code === 0) {
                $(gridid).jsGrid("refresh");
                dialogmgr.hide();
            } else {
                alert(result.Message);
            }

        })
        .fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    };

    function BindEvents() {
    }
    function fillDatatable() {
        $(gridid).jsGrid({
            width: "100%",
            height: "auto",
            filtering: true,
            editing: true,
            inserting: false,
            sorting: true,
            paging: true,
            pageSize: 20,
            autoload: true,
            controller: {
                loadData: function (filter) {
                    var d = $.Deferred();
                    $.ajax({
                        url: actionloaddata,
                        type: "POST",
                        data: filter,
                        //Output
                        dataType: "json"
                    }).done(function (response) {
                        d.resolve(response);
                    }).fail(function (obj) {
                        var res = 1;
                        res = 2;
                    });
                    return d.promise();
                },
                deleteItem: function (args) {
                    var f = $.Deferred();
                    $.ajax({
                        url: actiondelete,
                        type: "POST",
                        data: args,
                        //output
                        dataType: "json"
                    }).done(function (response) {
                        $(gridid).jsGrid("loadData");
                    });
                    return f.promise();
                },
                editItem: function (args) {
                    alert(args);
                }
            },
            onItemEditing: function (args) {
                // cancel editing of the row of item with field 'ID' = 0
                args.cancel = true;
                showDetailsDialog("Edit Foliar Sampling", args.item);
            },
            rowClick: function (args) {
                args.cancel = true;
                //showDetailsDialog("Edit", args.item);
            },
            rowClass: function (item, itemIndex) {
                var $result = $([]);
                if (2 === item.dataStatus) {
                    return "jsgridreadOnlyRow";
                }
                //if(item.readOnly) {
                //  $result = $result.add(this._createEditButton(item));
                //}
            },
            fields: [
                { name: "FLSM_PLOT_ID", type: "text", title: "FLSM PLOT ID", width: "120px" },
                { name: "FLSM_TREE_ID", type: "text", title: "FLSM TREE ID", width: "120px" },
                { name: "FLSM_SAMPLE_ID", type: "text", title: "FLSM SAMPLE ID", width: "160px" },
                {
                    type: "control",
                    headerTemplate: function () {
                        return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                                .on("click", function () {
                                    showDetailsDialog("Add Foliar Sampling", null);
                                });
                    },
                    itemTemplate: function (value, item) {
                        var $result = $([]);
                        if (item.dataStatus == 0) {
                            $result = $result.add(this._createEditButton(item));
                            $result = $result.add(this._createDeleteButton(item));
                        }
                        return $result;
                    }
                }
            ],
            deleteConfirm:function(item){
            }
        });
    }
    function showDetailsDialog(action, data) {
        dialogmgr = new dialogMgr(dialogid, action);
        dialogmgr.initializeDialog(foliarSamplingMgr.prototype.confirmCallBack, actiondlg, data);
        dialogmgr.show();
    }
    function Init() {
        fillDatatable();
    }
    function format(str, args) {
        var internalstr = str;
        for (i = 0; i < args.length; i++)
            internalstr = internalstr.replace("{" + i + "}", args[i]);
        return internalstr;
    }
    return foliarSamplingMgr;
}());
