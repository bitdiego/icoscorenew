﻿var teamMemberDialog=(function() {
  let dialogid="";
  let actionSave="";
  let actionUpdate="";
  let actionGet="";
  let actionRetrieveDialog="";
  let currentActionReq=0;
  let delegatebtnSave;
  let dataInput=null;
  function teamMemberDialog(iddialog,dialogTitle) {
    dialogid=iddialog;
    if(undefined!==dialogTitle && null!==dialogTitle)$(dialogid+" #title").text(dialogTitle);
  }
  teamMemberDialog.prototype.initializeDialog = function (delegatebtnsave, actionretrievedialog, currentactionreq, data) {
      delegatebtnSave = delegatebtnsave;
      actionRetrieveDialog = actionretrievedialog;
      currentActionReq=currentactionreq;
      dataInput=data;
      Init();
  };
  teamMemberDialog.prototype.show = function () {
      $(dialogid).modal('show');
  };
  teamMemberDialog.prototype.hide= function () {
      $(dialogid).modal('hide');
  };
  //private methods
  function BindEvents() {
    //Events
    $(dialogid+" #btnsave").on('click',function(e) {
      if($("#formInput").valid()){
        values=serializeToArray('formInput');
        delegatebtnSave(values,currentActionReq,dataInput.TEAM_MEMBER_ID);
        //$(dialogid+" #btnsave").off('click');
      }else{
        return false;
      }
    });
    $("a .linkUpdate").on('click',function() {
      var itemId=$(this).attr('data-itemid');
      //alert("Il valore dell'id è ".concat(itemId));
    });
    //events
  }
  function Init() {
    //Controls
    $.ajax({
      url: actionRetrieveDialog,
      data: {
          model:dataInput,
      },
      cache:false,
      type:null===dataInput?"GET":"POST",
        beforeSend: function() {
          //imposta il loader
          $("#bodyLoader").show();
        },
    }).done(function (data) {
        $("#dlgStruct div[class='modal-body']").html(data);
        BindEvents();
      })
      .always(function() {
        //devo nascondere il loader...
        $("#bodyLoader").hide();
        let datepickerObj=new datepickerMgr();
        datepickerObj.InitiliazeDatePickers('TEAM_MEMBER_DATE',true);
      });
  }
  function serializeToArray(idElement) {
    var result;
    result=JSON.parse(JSON.stringify($("#"+idElement).find(':not(:checkbox)').serializeArray()));
    $("#"+idElement+" input:checkbox").each(function() {
      result.push({ name: this.name,value: this.checked });
    });
    var formData = $(this).serialize();
    return result;
  }
  return teamMemberDialog;
}());
