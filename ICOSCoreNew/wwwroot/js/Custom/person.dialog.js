﻿var personDialog = (function () {
    let dialogid = "";
    let actionSave = "";
    let actionUpdate = "";
    let actionGet = "";
    let actionRetrieveDialog = "";
    let currentActionReq = 0;
    let delegatebtnSave;
    let dataInput = null;
    function personDialog(iddialog, dialogTitle) {
        dialogid = iddialog;
        if (undefined !== dialogTitle && null !== dialogTitle) $(dialogid + " #title").text(dialogTitle);
    }
    personDialog.prototype.initializeDialog = function (delegatebtnsave, actionretrievedialog, currentactionreq, data) {
        delegatebtnSave = delegatebtnsave;
        actionRetrieveDialog = actionretrievedialog;
        currentActionReq = currentactionreq;
        dataInput = data;
        Init();
    };
    personDialog.prototype.show = function () {
        $(dialogid).modal();
    };
    //private methods
    function BindEvents() {
        //Events
        $(dialogid + " #btnsave").on('click', function (e) {
            values = serializeToArray('detailsForm');
            delegatebtnSave(values);
            $(dialogid + " #btnsave").off('click');
        });
        $("a .linkUpdate").on('click', function () {
            var itemId = $(this).attr('data-itemid');
        });
        //events
    }
    function Init() {
        //Controls
        //BindEvents();
        debugger;
        $.ajax({
            url: actionRetrieveDialog,
            data: {
                tm: dataInput,
                teamname: null !== dataInput ? dataInput.TEAM_MEMBER_NAME : null
            },
            cache: false,
            type: "POST",
            beforeSend: function () {
                //imposta il loader
                $("#bodyLoader").show();
            },
        }).done(function (data) {
            debugger;
            $("#dlgStruct div[class='modal-body']").html(data);
        })
            .always(function () {
                //devo nascondere il loader...
                $("#bodyLoader").hide();
                let datepickerObj = new datepickerMgr();
                datepickerObj.InitiliazeDatePickers('TEAM_MEMBER_DATE', true);
            });
    }
    function serializeToArray(idElement) {
        // Il comportamento default del metodo jquery 'serializeArray' non riporta nell'insieme restituito
        // gli elementi HTML checkbox non selezionati. 
        // Con questa funzione obbligo ad inserire nell'insieme anche i controlli checkbox non selezionati,
        // ritornando un valore booleano.
        var result;

        result = JSON.parse(JSON.stringify($("#" + idElement).find(':not(:checkbox)').serializeArray()));
        $("#" + idElement + " input:checkbox").each(function () {
            result.push({ name: this.name, value: this.checked });
        });

        return result;
    }
    return personDialog;
}());
