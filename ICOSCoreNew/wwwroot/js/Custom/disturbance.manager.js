﻿var disturbanceMgr = (function () {
    let gridid = "";
    let dialogid = "";
    let actionsave = "";
    let actionupdate = "";
    let actiondelete = "";
    let actionloaddata = "";
    let actiondlg = "";

    function disturbanceMgr(gridId, idDialog, actionSave, actionUpdate, actionDelete, actionLoadData, actionDlg) {
        gridid = gridId;
        dialogid = idDialog;
        actionsave = actionSave;
        actionupdate = actionUpdate;
        actiondelete = actionDelete;
        actionloaddata = actionLoadData;
        actiondlg = actionDlg;
    }
    disturbanceMgr.prototype.Initialize = function () {
        Init();
    };
    disturbanceMgr.prototype.confirmCallBack = function (indata) {
        let action = "";
        let inputdata = indata.data === undefined ? indata : indata.data.oreGiornata;
        let currentactionreq = actionsave;
        //in questo caso devo aggiungere una riga alla fine della tabella
        $.ajax({
            url: currentactionreq,
            dataType: 'json',
            //contentType: 'application/json',
            data: inputdata,
            method: "POST",
            timeout: 0
        })
        .done(function (result, textStatus) {
            if (result.ReturnValue === 0) {
                $(gridid).refresh();
            } else {
                alert(result.Message);
            }

        })
        .fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    };

    function BindEvents() {
    }
    function fillDatatable() {
        $(gridid).jsGrid({
            width: "100%",
            height: "auto",
            filtering: true,
            editing: true,
            inserting: false,
            sorting: true,
            paging: true,
            pageSize: 20,
            autoload: true,
            controller: {
                loadData: function (filter) {
                    var d = $.Deferred();
                    $.ajax({
                        url: actionloaddata,
                        type: "POST",
                        data: filter,
                        //Output
                        dataType: "json"
                    }).done(function (response) {
                        d.resolve(response);
                    }).fail(function (obj) {
                        var res = 1;
                        res = 2;
                    });
                    return d.promise();
                },
                deleteItem: function (args) {
                    var f = $.Deferred();
                    $.ajax({
                        url: actiondelete,
                        type: "POST",
                        data: args,
                        //output
                        dataType: "json"
                    }).done(function (response) {
                        $(gridid).jsGrid("loadData");
                    });
                    return f.promise();
                },
                editItem: function (args) {
                    alert(args);
                }
            },
            onItemEditing: function (args) {
                // cancel editing of the row of item with field 'ID' = 0
                args.cancel = true;
                showDetailsDialog("Edit Disturbance Management", args.item);
            },
            rowClick: function (args) {
                args.cancel = true;
            },
            rowClass: function (item, itemIndex) {
                var $result = $([]);
                if (2 === item.dataStatus) {
                    return "jsgridreadOnlyRow";
                }
            },
      fields: [
          { name: "DM_AGRICOLTURE_DESCRIPTION", type: "text", title: "DM_AGRICOLTURE", width: "120px" },
          { name: "DM_ENCROACH_DESCRIPTION", type: "text", title: "DM_ENCROACH", width: "120px" },
          { name: "DM_EXT_WEATHER_DESCRIPTION", type: "text", title: "DM_EXT_WEATHER", width: "160px" },
          { name: "DM_FERT_M_DESCRIPTION", type: "text", title: "DM_FERT_M", width: "160px" },
          { name: "DM_FERT_O_DESCRIPTION", type: "text", title: "DM_FERT_O", width: "160px" },
          { name: "DM_FIRE_DESCRIPTION", type: "text", title: "DM_FIRE", width: "160px" },
          {
            type: "control",
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                      .on("click",function() {
                          showDetailsDialog("Add Disturbance Management", null);
                      });
            },
            itemTemplate: function(value,item) {
              var $result=$([]);
              if (item.dataStatus == 0) {
                  $result = $result.add(this._createEditButton(item));
                  $result = $result.add(this._createDeleteButton(item));
              } return $result;
            }
          }
      ],
    });
  }
    function showDetailsDialog(action, data) {
        var dialogmgr = new dialogMgr(dialogid, action);
        dialogmgr.initializeDialog(disturbanceMgr.prototype.confirmCallBack, actiondlg, data, ['DM_DATE_START', 'DM_END_START']);
        dialogmgr.show();
    }
    function Init() {
        fillDatatable();
    }
    function format(str, args) {
        var internalstr = str;
        for (i = 0; i < args.length; i++)
            internalstr = internalstr.replace("{" + i + "}", args[i]);
        return internalstr;
    }
    return disturbanceMgr;
}());
