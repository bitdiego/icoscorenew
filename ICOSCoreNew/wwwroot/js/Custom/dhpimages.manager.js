﻿var dhpImagesMgr=(function() {
  let gridid="";
  let dialogid="";
  let actionsave="";
  let actionupdate="";
  let actiondelete="";
  let actionloaddata="";
  let actiondlg="";

  function dhpImagesMgr(gridId,idDialog,actionSave,actionUpdate,actionDelete,actionLoadData,actionDlg) {
    gridid=gridId;
    dialogid=idDialog;
    actionsave=actionSave;
    actionupdate=actionUpdate;
    actiondelete=actionDelete;
    actionloaddata=actionLoadData;
    actiondlg=actionDlg;
  }
  dhpImagesMgr.prototype.Initialize=function() {
    Init();
  };
  dhpImagesMgr.prototype.confirmCallBack=function(indata) {
    let action="";
    let inputdata=indata.data===undefined?indata:indata.data.oreGiornata;
    let currentactionreq=actionsave;
    //in questo caso devo aggiungere una riga alla fine della tabella
    $.ajax({
      url: currentactionreq,
      dataType: 'json',
      //contentType: 'application/json',
      data: inputdata,
      method: "POST",
      timeout: 0
    })
    .done(function(result,textStatus) {
      if(result.ReturnValue===0) {
        $(gridid).refresh();
      } else {
        alert(result.Message);
      }

    })
    .fail(function(jqXHR,textStatus) {
      alert("Request failed: "+textStatus);
    });
  };

  function BindEvents() {
  }
  function fillDatatable() {
    $(gridid).jsGrid({
      width: "100%",
      height: "auto",
      filtering: true,
      editing: true,
      inserting: false,
      sorting: true,
      paging: true,
      pageSize: 20,
      autoload: true,
      controller: {
        loadData: function(filter) {
          var d=$.Deferred();
          $.ajax({
            url: actionloaddata,
            type: "POST",
            data: filter,
            //Output
            dataType: "json"
          }).done(function(response) {
            d.resolve(response);
          });

          return d.promise();
        },
        deleteItem: function(args) {
          var f=$.Deferred();
          $.ajax({
            url: actiondelete,
            type: "POST",
            data: args,
            //output
            dataType: "json"
          }).done(function(response) {
            $(gridid).jsGrid("loadData");
          });
          return f.promise();
        },
        editItem: function(args) {
          alert(args);
        }
      },
      onItemEditing: function(args) {
        // cancel editing of the row of item with field 'ID' = 0
        args.cancel=true;
        showDetailsDialog("Edit Dhp Images", args.item);
      },
      rowClick: function(args) {
        args.cancel=true;
      },
      rowClass: function(item,itemIndex) {
        var $result=$([]);
        if(0===item.dataStatus) {
          return "jsgridreadOnlyRow";
        }
        //if(item.readOnly) {
        //  $result = $result.add(this._createEditButton(item));
        //}
      },
      fields: [
          { name: "DHP_ID",type: "text",title: "DHP_ID",width: "120px" },
          { name: "DHP_CAMERA_DESCRIPTION", type: "text", title: "DHP_CAMERA", width: "120px" },
          { name: "DHP_CAMERA_SN",type: "text",title: "DHP_CAMERA_SN",width: "160px" },
          { name: "DHP_LENS_DESCRIPTION", type: "text", title: "DHP_LENS", width: "150px", filtering: false },
          { name: "DHP_LENS_SN",type: "text",title: "DHP_LENS_SN",width: "160px" },
          { name: "DHP_OC_ROW",type: "text",title: "DHP_OC_ROW",width: "120px" },
          {
            type: "control",
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                      .on("click",function() {
                          showDetailsDialog("Add Dhp Images", null);
                      });
            },
            itemTemplate: function(value,item) {
              var $result = $([]);
              if (item.dataStatus == 0) {
                  $result = $result.add(this._createEditButton(item));
                  $result = $result.add(this._createDeleteButton(item));
              }
              return $result;
            }
          }
      ],
    });
  }
  function showDetailsDialog(action,data) {
    var dialogmgr=new dialogMgr(dialogid,action);
    dialogmgr.initializeDialog(dhpImagesMgr.prototype.confirmCallBack, actiondlg, data);
    dialogmgr.show();
  }
  function Init() {
    fillDatatable();
  }
  function format(str,args) {
    var internalstr=str;
    for(i=0;i<args.length;i++)
      internalstr=internalstr.replace("{"+i+"}",args[i]);
    return internalstr;
  }
  return dhpImagesMgr;
}());
