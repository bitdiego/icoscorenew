﻿var towerMgr=(function() {
  let gridid="";
  let dialogid="";
  let actionsave="";
  let actionupdate="";
  let actiondelete="";
  let actionloaddata="";
  let actiondlg="";

  function towerMgr(gridId,idDialog,actionSave,actionUpdate,actionDelete,actionLoadData,actionDlg) {
    gridid=gridId;
    dialogid=idDialog;
    actionsave=actionSave;
    actionupdate=actionUpdate;
    actiondelete=actionDelete;
    actionloaddata=actionLoadData;
    actiondlg=actionDlg;
  }
  towerMgr.prototype.Initialize=function() {
    Init();
  };
  towerMgr.prototype.confirmCallBack=function(indata) {
    let action="";
    let inputdata=indata.data===undefined?indata:indata.data.oreGiornata;
    let currentactionreq=actionsave;
    //in questo caso devo aggiungere una riga alla fine della tabella
    $.ajax({
      url: currentactionreq,
      dataType: 'json',
      //contentType: 'application/json',
      data: inputdata,
      method: "POST",
      timeout: 0
    })
    .done(function(result,textStatus) {
      if(result.ReturnValue===0) {
        $(gridid).refresh();
      } else {
        alert(result.Message);
      }

    })
    .fail(function(jqXHR,textStatus) {
      alert("Request failed: "+textStatus);
    });
  };

  function BindEvents() {
  }
  function fillDatatable() {
    $(gridid).jsGrid({
      width:"100%",
      height: "auto",
      filtering: true,
      editing: true,
      inserting: false,
      sorting: true,
      paging: true,
      pageSize: 20,
      autoload: true,
      controller: {
        loadData: function(filter) {
          var d=$.Deferred();
          $.ajax({
            url: actionloaddata,
            type: "POST",
            data: filter,
            //Output
            dataType: "json"
          }).done(function(response) {
            d.resolve(response);
          });

          return d.promise();
        },
        deleteItem: function(args) {
          var f=$.Deferred();
          $.ajax({
            url: actiondelete,
            type: "POST",
            data: args,
            //output
            dataType: "json"
          }).done(function(response) {
            $(gridid).jsGrid("loadData");
          });
          return f.promise();
        },
        editItem: function(args) {
          alert(args);
        }
      },
      onItemEditing: function(args) {
        // cancel editing of the row of item with field 'ID' = 0
        args.cancel=true;
        showDetailsDialog("Edit Tower",args.item);
      },
      rowClick: function(args) {
        args.cancel=true;
      },
      rowClass: function(item,itemIndex) {
        var $result=$([]);
        if(2===item.dataStatus) {
          return "jsgridreadOnlyRow";
        }
      },
      fields: [
          { name: "TOWER_TYPE",type: "text",title: "TOWER_TYPE",width: "120px" },
          { name: "TOWER_HEIGHT",type: "text",title: "TOWER_HEIGHT",width: "120px" },
          { name: "TOWER_ACCESS",type: "text",title: "TOWER_ACCESS",width: "120px" },
          { name: "TOWER_POWER",type: "text",title: "TOWER_POWER",width: "120px" },
          { name: "TOWER_POWER_AVAIL",type: "text",title: "TOWER_POWER_AVAIL",width: "120px" },
          { name: "TOWER_DATATRAN",type: "text",title: "TOWER_DATATRAN",width: "120px" },
          {
            type: "control",
            headerTemplate: function() {
              return $("<button>").attr("type", "button").attr("class", "button rounded fill-highlight").text("Add")
                      .on("click",function() {
                        showDetailsDialog("Add Tower",null);
                      });
            },
            itemTemplate: function(value,item) {
                var $result = $([]);
                if (item.dataStatus == 0) {
                    $result = $result.add(this._createEditButton(item));
                    $result = $result.add(this._createDeleteButton(item));
                }
                return $result;
            }
          }
      ],
    });
  }
  function showDetailsDialog(action,data) {
    var dialogmgr=new dialogMgr(dialogid,action);
    dialogmgr.initializeDialog(towerMgr.prototype.confirmCallBack,actiondlg,data);
    dialogmgr.show();
  }
  function Init() {
    fillDatatable();
  }
  function format(str,args) {
    var internalstr=str;
    for(i=0;i<args.length;i++)
      internalstr=internalstr.replace("{"+i+"}",args[i]);
    return internalstr;
  }
  return towerMgr;
}());
