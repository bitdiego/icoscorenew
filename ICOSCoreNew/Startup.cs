﻿using System;
using ICOSCoreNew.DB;
using ICOSCoreNew.Models.DataServices;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.CloudServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using ICOSCoreNew.Models.Utils;
using ICOSCoreNew.Models.BADM;
using ICOSCoreNew.Models.DTO;

namespace ICOSCoreNew
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDistributedMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(10);//You can set Time   
            });


            services.AddTransient<IDataStorageServices, EFDataServices>();
            services.AddTransient<ICloudServices, CloudServices>();
            services.AddDbContext<ICOSEntities>(options =>
                                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IUserLoginServices, AccountDA>();
            services.AddScoped<ISitesListService, SitesListService>();
            services.AddScoped<IVariableAdapter<LocationGroup>, LocationGroupAdapter>();

            services.AddMvc();
           // services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                                .AddJsonOptions(options => {
                                    var resolver = options.SerializerSettings.ContractResolver;
                                    if (resolver != null)
                                        (resolver as DefaultContractResolver).NamingStrategy = null;
                                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
