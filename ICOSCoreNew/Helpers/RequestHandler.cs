﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ICOSCore.Helpers
{
    public class RequestHandler
    {
        IHttpContextAccessor _httpContextAccessor;
        public HttpContext Context { get { return _httpContextAccessor.HttpContext; } set { } }
        public RequestHandler(IHttpContextAccessor accessor)
        {
            _httpContextAccessor = accessor;
        }

        internal void HandleAboutRequest()
        {
            // handle the request
            var message = "The HttpContextAccessor seems to be working!!";
            _httpContextAccessor.HttpContext.Session.SetString("message", message);
        }

        
    }
}
