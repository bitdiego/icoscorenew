﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ICOSCoreNew.DB;
using System.IO;
using System.Reflection;
using ICOSCoreNew.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using ICOSCoreNew.Models.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.ViewModel;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using Microsoft.Extensions.Configuration;
using ICOSCoreNew.Models.Utils;

namespace ICOSCoreNew.Controllers
{
    public class PIController : Controller
    {
        private readonly IDataStorageServices iDataStorageService;
        private readonly IConfiguration configuration;
       // private ICOS_site icos_site;

        public PIController(IDataStorageServices _ids, IConfiguration iConfig)
        {
            this.iDataStorageService = _ids;
            this.configuration = iConfig;
         //   icos_site = new ICOS_site();
        }
        public IActionResult Dashboard()
        {
            if ( AccountDA.CurrentUserID != 0)
            {
                if (AccountDA.CurrentICOS == 1)
                    //TODO
                    //return View(PiDA.GetTimelineEventsAndAlerts(70));
                return View();
                else
                    return RedirectToAction("ChangePassword", "Account");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public async Task<IActionResult> Fileupload()
        {
            ICOSSitePiViewModel viewModel = await iDataStorageService.GetPISitesViewModelListAsync();
            //viewModel = await GetPISitesListAsync();

            return View(viewModel);
        }

        /*public ActionResult TimeLineTest()
        {

            return View(PiDA.GetTimelineEventsAndAlerts(438));
        }

        public void SubmitTracknr(string eventId, string tracknr, string dateSent)
        {
            PiDA.SubmitTracknr(eventId, tracknr, dateSent);
        }
        public ActionResult Team()
        {
            return View();
        }
        public ActionResult Location()
        {
            return View();
        }

        public ActionResult Utc()
        {
            return View();
        }
        public ActionResult LandOwnership()
        {
            return View();
        }
        public ActionResult Tower()
        {
            return View();
        }
        public ActionResult Tree()
        {
            return View();
        }

        public ActionResult SPP()
        {
            return View();
        }

        public ActionResult Litter()
        {
            return View();
        }

        public ActionResult AGB()
        {
            return View();
        }
        public ActionResult BULK()
        {
            return View();
        }

        public ActionResult GAI()
        {
            return View();
        }

        public ActionResult CEPT()
        {
            return View();
        }
        public ActionResult ClimateAverage()
        {
            return View();
        }
        public ActionResult SamplingPlots()
        {
            return View();
        }
        public ActionResult FoliarSampling()
        {
            return View();
        }
        public ActionResult SoilSampling()
        {
            return View();
        }
        public ActionResult Dhp()
        {
            return View();
        }
        public ActionResult InstrumentalModel()
        {
            return View();
        }
        public ActionResult Logger()
        {
            return View();
        }
        public ActionResult FileModel()
        {
            return View();
        }
        public ActionResult EddyCovariance()
        {
            return View();
        }
        public ActionResult EcSystem()
        {
            return View();
        }
        public ActionResult MeteoMes()
        {
            return View();
        }
        public ActionResult StorageGroup()
        {
            return View();
        }
        public ViewResult Disturbance()
        {
            return View();
        }
        */
        
        
        
        /* */


        [HttpPost]
    public async Task<string> Save(List<IFormFile> files, string fileType, string site, string dataInfo, bool sampleCheck=false)
    {
        string message = "";
        string bodyMail = "A new $FF$ file has been submitted from the ICOS Upload web page\n"; ;
        string submittedFileType = "";
        //containing the files to attach to mail recipients
        List<string> filesToPost = new List<string>();
        //containing the mail recipients to send in CC
        List<string> lcc = new List<string>();
        if (!ModelState.IsValid)
        {
            return "An error occurred. Please contact administrators<br />info@icos-etc.eu";// RedirectToAction("Index");
        }
        else
        {
            //context = new ICOSEntities();
            int fType = int.Parse(fileType);
            int piSession = AccountDA.CurrentUserID;
            int siteID = 0; //iDataStorageService.GetSiteIdBySiteCode(site);//  context.ICOS_site.Where(sn => sn.site_code == site).Select(sn => sn.id_icos_site).SingleOrDefault();
            short? siteClass = 0; //context.ICOS_site.Where(sn => sn.id_icos_site == siteID).Select(sn => sn.@class).SingleOrDefault();
            iDataStorageService.GetSiteIdClassBySiteCode(site, ref siteID, ref siteClass);
            string newFileName = "";
            foreach (IFormFile file in files)
            {
                if (file != null)
                {

                        //ALL THE SAVEAS...ACTIONS FOR FILES I THINK NEED TO BE REWRITTEN IN ORDER TO save AS BLOB DATA TYPE....
                        //PROBLEM: SOME FILES NEED TO BE SENT TO RECIPIENTS IN ATTACHMENTS: SEE BELOW ON fType switch
                        //DIEGO:: end 
                        var path = Path.Combine(configuration.GetSection("DefaultUploadFolder").Value, file.FileName);

                    switch (fType)
                    {
                        case 12:
                                //BADM file: save to storage
                                //file.SaveAs(path);
                            newFileName = SetNewFileName(file.FileName);
                            message += "Processing " + file.FileName + "<br />";
                            submittedFileType = "BADM";
                            using (var stream = new FileStream(Path.Combine(configuration.GetSection("DefaultUploadFolder").Value, newFileName), FileMode.Create))
                            {
                                await file.CopyToAsync(stream);
                                message += "File succesfully uploaded<br />";
                            }
                                
                                break;
                        case 13:
                            //kml file: send attached to g.nicolini@unitus.it and create CP_SP file
                            if (sampleCheck)
                            {
                                    //check if CP points have been submitted
                                bool isCP = await iDataStorageService.CPRecordsExistAsync((int)Globals.BadmVarsGroups.GRP_PLOT, siteID, "CP_");
                                if (!isCP)
                                {
                                    return "WARNING! CP points coordinates are missing. Please submit by BADM Sampling Scheme Template or by the Web UI";
                                }

                                 //* create a txt file with ec-cp coordinates
                                 //fToJack = @"F:\icos\" + sitesList.SelectedItem.Text + "\\" + sitesList.SelectedItem.Text + "_EC-CP_coordinates.txt";
                                // getCooFile(sitesList.SelectedItem.Text, true, fToJack);

                                bodyMail += "\n\nSubmitter asked to treat the CPs as Exclusion Areas";
                            }

                            //file.SaveAs(path);
                           // string fToJack= Path.Combine(Server.MapPath("~/App_Data"), site + "_EC-CP_coordinates.txt");
                            string fToJack = Path.Combine(@"C:\Users\DiegoPolidori\source\repos\icoscore\ICOSCore\wwwroot\upload", site + "_EC -CP_coordinates.txt");
                            filesToPost.Add(fToJack);
                            lcc.Add("g.nicolini@unitus.it");
                            submittedFileType = "KML Target/Exclusion area";
                            break;
                        case 14:
                            //zip shape: send attached to g.nicolini@unitus.it and create CP_SP file
                            //file.SaveAs(path);
                            int shpFlag = 0;
                            submittedFileType = "zip Shape";
                            string taReg = @"^[a-zA-Z]{2}\-[a-zA-Z0-9]{3}_(TA).[a-zA-Z]{3}$";
                            string eaReg = @"[a-zA-Z]{2}\-[a-zA-Z0-9]{3}_(EA)([0][1-9]|[1][0-9]|20).[a-zA-Z]{3}$";
                            message += "Processing " + file.FileName  + "<br />";
                            string fExt = file.FileName.Substring(file.FileName.LastIndexOf(".") + 1);
                            string _fNameToChange = file.FileName.Substring(0, file.FileName.LastIndexOf("."));
                            ZipFile zf = new ZipFile(file.OpenReadStream());
                            string shp = _fNameToChange + ".shp";
                            string shx = _fNameToChange + ".shx";
                            string dbf = _fNameToChange + ".dbf";
                            string prj = _fNameToChange + ".prj";
                            List<string> items = new List<string>();
                            foreach (ZipEntry zipEntry in zf)
                            {
                                if (!zipEntry.IsFile)
                                {
                                    continue;
                                }
                                else
                                {
                                    var matches = Regex.Matches(zipEntry.Name, site);
                                    string _zip = zipEntry.Name;
                                    if (matches.Count > 1)
                                    {
                                        _zip = zipEntry.Name.Substring(_zip.IndexOf("/") + 1);
                                    }
                                    Match m1 = Regex.Match(_zip, taReg, RegexOptions.IgnoreCase);
                                    Match m2 = Regex.Match(_zip, eaReg, RegexOptions.IgnoreCase);
                                    if (!m1.Success && !m2.Success)
                                    {
                                        message += "<br />Warning: file name " + _zip + " has a wrong format!<br />";
                                    }
                                    else
                                    {
                                        message += "Found " + _zip + " in zip file<br />";
                                        items.Add(_zip);

                                    }

                                }

                            }
                            string s1 = items.Find(xp => xp.IndexOf("shp") >= 0);
                            if (s1 == null)
                            {
                                message += "Not Found .shp file<br />";
                                message += "Errors in " + _fNameToChange + "." + fExt + "<br />";
                                shpFlag++;
                                //return;
                            }
                            s1 = items.Find(xp => xp.IndexOf("shx") >= 0);
                            if (s1 == null)
                            {
                                message += "Not Found .shx file<br />";
                                message += "Errors in " + _fNameToChange + "." + fExt + "<br />";
                                shpFlag++;
                                //return;
                            }
                            s1 = items.Find(xp => xp.IndexOf("prj") >= 0);
                            if (s1 == null)
                            {

                                message += "Not Found .prj file<br />";
                                message += "Errors in " + _fNameToChange + "." + fExt + "<br />";
                                shpFlag++;
                                // return;
                            }
                            s1 = items.Find(xp => xp.IndexOf("dbf") >= 0);
                            if (s1 == null)
                            {
                                message += "Not Found .dbf file<br />";
                                message += "Errors in " + _fNameToChange + "." + fExt + "<br />";
                                shpFlag++;
                                // return;
                            }
                            if (shpFlag > 0)
                            {
                                return message;
                            }

                            break;
                        case 15:
                            //file.SaveAs(path);
                            submittedFileType = "Other files";
                            //bodyMail += Environment.NewLine + dangerRemove(TextBoxinfo.Text.Trim());
                            break;
                        case 16:
                            //file.SaveAs(path);
                            lcc.Add("maarten.opdebeeck@uantwerpen.be");
                            lcc.Add("bert.gielen@uantwerpen.be");
                            submittedFileType = "Field Map";
                            break;
                        case 17:
                           // file.SaveAs(path);
                            lcc.Add("maarten.opdebeeck@uantwerpen.be");
                            lcc.Add("bert.gielen@uantwerpen.be");
                            submittedFileType = "Ceptometer data";
                            break;
                        case 18:
                            // Header file
                            submittedFileType = "Header file";
                            newFileName = file.FileName;
                                
                            using (var stream = new FileStream(Path.Combine(configuration.GetSection("DefaultUploadFolder").Value, newFileName), FileMode.Create))
                            {
                                await file.CopyToAsync(stream);
                                message += "File succesfully uploaded<br />";
                            }

                            if (file.FileName.ToLower().IndexOf("bm") >= 0 )
                            {
                                //checkBM(sitesList.SelectedItem.Text, file.FileName, 70);
                            }
                            lcc.Add("simone.sabbatini@unitus.it");
                            lcc.Add("domvit@unitus.it");
                            break;
                        case 19:
                            submittedFileType = "Continuous data file";
                            if (siteClass != 0)
                            {
                                message += "Warning! Only associated stations can upload continuous data files<br />";
                                return message;
                            }
                            //file.SaveAs(path);
                            break;
                        case 20:
                            submittedFileType = "DHP image";
                            if (file.FileName.EndsWith(".zip", StringComparison.CurrentCultureIgnoreCase))
                            {
                                message += "Processing " + file.FileName + "<br />";
                                string dhpReg = @"[a-zA-Z]{2}\-[a-zA-Z0-9]{3}_(DHP)_(S)[1-9]_(CP|SP)([0][1-9]|[1][0-9]|20)_(L)([0][1-9]|[1][0-9]|20)_[0-9]{8}.[a-zA-Z0-9]{3,4}$";
                                ZipFile zz = new ZipFile(file.OpenReadStream());
                                foreach (ZipEntry zipEntry in zz)
                                {
                                    if (!zipEntry.IsFile)
                                    {
                                        if (zipEntry.IsDirectory)
                                        {
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Match match = Regex.Match(zipEntry.Name, dhpReg, RegexOptions.IgnoreCase);
                                        if (!match.Success)
                                        {
                                            message += "Error:: " + zipEntry.Name + " has a wrong name!!!<br />";
                                            continue;
                                        }
                                        Stream zipStream = zz.GetInputStream(zipEntry);

                                       // String fullZipToPath = Path.Combine(Server.MapPath("~/App_Data"), zipEntry.Name);
                                            String fullZipToPath = Path.Combine(@"C:\Users\DiegoPolidori\source\repos\icoscore\ICOSCore\wwwroot\upload", zipEntry.Name);
                                            byte[] buffer = new byte[4096];
                                        FileInfo info = new FileInfo(fullZipToPath);
                                        using (FileStream streamWriter = info.Create()) //
                                        {
                                            StreamUtils.Copy(zipStream, streamWriter, buffer);
                                            message += zipEntry.Name + " succesfully uploaded<br />";
                                            //insert each item in database, table icosfiles
                                            //upload each item in cloud storage
                                            //insertIfiles(zipEntry.Name, zipEntry.Name, int.Parse(sitesList.SelectedValue), int.Parse(Session["idutente"].ToString()), dangerRemove(TextBoxinfo.Text), choice);
                                        }

                                    }

                                }
                            }
                            else
                            {

                            }

                            break;
                        case 22:
                            //General images
                            submittedFileType = "General image file";
                            newFileName = SetNewFileName(file.FileName);

                            using (var stream = new FileStream(Path.Combine(configuration.GetSection("DefaultUploadFolder").Value, newFileName), FileMode.Create))
                            {
                                await file.CopyToAsync(stream);
                                message += "Image succesfully uploaded<br />";
                            }
                            break;
                        case 23:
                            //raw data
                            break;

                    }
                    bodyMail = bodyMail.Replace("$FF$", submittedFileType);
                    bodyMail += "\nFile name: " + file.FileName;

                    //Do not attach DHP, continuous data or Image files
                    if (fType != 20 && fType!=22 && fType != 19)
                    {
                        filesToPost.Add(path);
                    }
                    //DIEGO::::what does tihis do???
                    int x= await xup2Async(newFileName, site, null, false);
                    if (x != 0)
                    {
                        //context.Dispose();
                        return "An error occurred in saving file to cloud storage. Please contact administration (info@icos-etc.eu)";
                    }

                    System.IO.File.Delete(path);

                        //DIEGO::: TO SAVE IN DB!!!!!!!!!!
                    bool res = await  iDataStorageService.SaveFileInDbAsync(file.FileName, newFileName, siteID, fType, AccountDA.CurrentUserID);
                    if (!res)
                    {
                        return "An error occurred in saving file to database. Please contact administration (info@icos-etc.eu)";
                    }
                }
            }
        }
        //send mails...add submitter credentials to body mail
        //context.Dispose();
        return message;
    }

        private async Task<ICOSSitePiViewModel> GetPISitesListAsync()
        {
            List<IcosSitePI> _tList = await iDataStorageService.GetPISitesListAsync();
            ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
            viewModel.sitesPiList = new List<SelectListItem>();

            foreach (var _t in _tList)
            {
                viewModel.sitesPiList.Add(new SelectListItem(_t.site_code, _t.id_site.ToString()));
            }

            return viewModel;
        }


        public async Task<int> xup2Async(IFormFile file, string site, string nn)
        {
            var _root = configuration.GetSection("DefaultUploadFolder").Value;
            var prefix = "icos/" + site + "/";

            try
            {

                string blobName = "";// prefix + SetNewFileName(file.FileName);
                if (!String.IsNullOrEmpty(nn))
                {
                    blobName = prefix + nn;
                }
                else
                {
                    blobName = prefix + SetNewFileName(file.FileName);
                }

                var storageCredentials = new StorageCredentials("icos", "KnyTe3J/vek3L53ygGPLhGbJvffagd5iOinY/lmRvA/VDMBfZ3d1yOIkUwY+cR/UxlZtA9nwGIqp/K5siBN+2A==");
                CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
                CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient(); //.CreateCloudBlobClient();
                CloudFileShare fileShare = fileClient.GetShareReference("icosshare");
                await fileShare.CreateIfNotExistsAsync();
                CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(blobName);
                await cloudFile.UploadFromFileAsync( Path.Combine(_root,  file.FileName));
            
                return 0;
            }
            catch (Exception e)
            {
                return 1;
            }
        }

        public async Task<int> xup2Async(string fileName, string site, string nn, bool isFieldbook)
        {
            var _root = configuration.GetSection("DefaultUploadFolder").Value;
            var prefix = "icos/" + site + "/";
            if (isFieldbook)
            {
                prefix += "fieldbook/";
            }
            try
            {

                string blobName = "";// prefix + SetNewFileName(file.FileName);
                if (!String.IsNullOrEmpty(nn))
                {
                    blobName = prefix + nn;
                }
                else
                {
                    blobName = prefix + fileName;//SetNewFileName(fileName);
                }
                var storageCredentials = new StorageCredentials(configuration.GetSection("AzureFileShare").GetValue<string>("user"),
                                                                configuration.GetSection("AzureFileShare").GetValue<string>("password"));
                CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
                CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient(); //.CreateCloudBlobClient();
                CloudFileShare fileShare = fileClient.GetShareReference(configuration.GetSection("AzureFileShare").GetValue<string>("shareName"));
                await fileShare.CreateIfNotExistsAsync();
                CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(blobName);
                await cloudFile.UploadFromFileAsync(Path.Combine(_root, fileName));

                return 0;
            }
            catch (Exception e)
            {
                return 1;
            }
        }

        /*
    public ActionResult PIMenu()
    {
        return View();
        //SelectorVM selectorVM = new SelectorVM();
        //int xId_site = -1;
        //if (System.Web.HttpContext.Current.Session["PiSite"] != null)
        //{
        //    Int32.TryParse(System.Web.HttpContext.Current.Session["PiSite"].ToString(), out xId_site);
        //}
        ////  selectorVM.Id_site = xId_site;

        //List<SelectListItem> items = new List<SelectListItem>();
        //bool xsel = (System.Web.HttpContext.Current.Session["PiSite"] == null);

        //items.Add(new SelectListItem { Text = "Please select", Value = "0" });
        //using (ICOSEntities context = new newIcos.ICOSEntities())
        //{
        //    var xitems = context.IcosSitePIS.Where(v => v.id_user == AccountDA.CurrentUserId).ToList();
        //    foreach (var item in xitems)
        //    {
        //        bool mensel = ((System.Web.HttpContext.Current.Session["PiSite"] != null) && (item.id_site.ToString() == System.Web.HttpContext.Current.Session["PiSite"].ToString()));
        //        items.Add(new SelectListItem { Text = item.site_code, Value = item.id_site.ToString(), Selected = (item.id_site == xId_site) });
        //    }
        //}

        //selectorVM.Id_site = xId_site;
        //selectorVM.SiteList = new SelectList(items, "Value", "Text");

        ////                    items.Add(new SelectListItem { Text = "site1", Value = "0" });
        //ViewBag.sitemenu = new SelectList(items, "Value", "Text");
        //return View(selectorVM);
    }
    */

        /* public async Task<IActionResult> SiteSelector()
     {
              SelectorVM selectorVM = new SelectorVM();
              int xId_site = -1;
              if (HttpContext.Session.GetString("PiSite") != null)
              {
                  Int32.TryParse(HttpContext.Session.GetString("PiSite"), out xId_site);
              }
              //  selectorVM.Id_site = xId_site;

              List<SelectListItem> items = new List<SelectListItem>();
              bool xsel = (HttpContext.Session.GetString("PiSite") == null);

                  List<IcosSitePI> sitesList = iDataStorageService.GetPISitesList();

                  foreach(var sl in sitesList)
                  {
                      items.Add(new SelectListItem { Text = sl.site_code, Value = sl.id_site.ToString(), Selected = (sl.id_site == xId_site) });
                  }
                  // try to simplify  items.Add(new SelectListItem { Text = "Please select", Value = "0" });
                  using (ICOSEntities context2 = new ICOSEntities(options))
                  {
                      //items.Add(new SelectListItem { Text = "Please select", Value = "0" });
                      var xitems = context2.IcosSitePIS.Where(v => v.id_user == AccountDA.CurrentUserID).OrderBy(s=>s.site_code).ToList();
                      foreach (var item in xitems)
                      {
                          bool mensel = ((HttpContext.Session.GetString("PiSite") != null) && (item.id_site.ToString() == HttpContext.Session.GetString("PiSite")));
                          items.Add(new SelectListItem { Text = item.site_code, Value = item.id_site.ToString(), Selected = (item.id_site == xId_site) });
                      }
                  }

                  // autoselectioon
                   if (xId_site==-1)
                   {
                       Int32.TryParse(items[0].Value, out xId_site);
                       HttpContext.Session.SetString("PiSite" ,items[0].Value);
                   }

                  //selectorVM.Id_site = 70;//xId_site;
              selectorVM.SiteList = new SelectList(items, "Value", "Text");

              //                    items.Add(new SelectListItem { Text = "site1", Value = "0" });
           //   ViewBag.sitemenu = new SelectList(items, "Value", "Text");
                  return View(selectorVM);
             List<IcosSitePI> _tList = await iDataStorageService.GetPISitesListAsync();
             ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
             viewModel.sitesPiList = new List<SelectListItem>();

             foreach (var _t in _tList)
             {
                 //viewModel.sitesPiList.Append(new SelectListItem(_t.site_code, _t.id_site.ToString()));
                 viewModel.sitesPiList.Add(new SelectListItem(_t.site_code, _t.id_site.ToString()));
             }

             return View("Fileupload", viewModel);
         }*/
        /*
            [HttpPost]
            public void UpdateSite(int xpisitesmenu)
            {
                // TODO: AC - wrap session in a property??!!!
                System.Web.HttpContext.Current.Session["PiSite"] = xpisitesmenu.ToString();
            }

            [HttpGet]
            public ActionResult SetSite(int? siteId)
            {
                System.Web.HttpContext.Current.Session["PiSite"] = siteId.ToString();

                return Content("");
            }
            [HttpGet]
            public ActionResult fileExists(string xfilename)
            {
                //System.Web.HttpContext.Current.Session["PiSite"] = siteId.ToString();

                string folder = xfilename.Substring(0, 6);
                int cx = 0;
                using (ICOSEntities context2 = new newIcos.ICOSEntities())
                {
                    cx = context2.icosfiles.Where(v => v.originalname == xfilename).ToList().Count ;
                }

                return Json(new { d = cx.ToString() }, JsonRequestBehavior.AllowGet);

        }
            public ActionResult Badmstart()
            {
                if (AccountDA.CurrentUserId != null && AccountDA.CurrentUserId != 0)
                {
                    if (AccountDA.CurrentICOS == true)
                    { 
                        //TODO
                        //return View(PiDA.GetTimelineEventsAndAlerts(70));
                        // takes custom html
                        ViewBag.HtmlStr = util.GetHtmlFromStorage("Areas", "1");

                        return View();
                    }
                    else
                        return RedirectToAction("ChangePassword", "Account");
                }
                else
                    return RedirectToAction("Login", "Account");
            }

            */

            public async  Task<IActionResult> Submittedfiles()
            {
                if (AccountDA.CurrentUserID != 0)
                {
                    if (AccountDA.CurrentICOS >0)
                    {

                    ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
                    viewModel = await GetPISitesListAsync();

                        return View(viewModel);
                    }
                    else
                        return RedirectToAction("ChangePassword", "Account");
                }
                else
                    return RedirectToAction("Login", "Account");
            }
        
            [HttpGet]
            public async Task<IActionResult> GetSubmittedFilesForSite(int siteid)
            {
                if ( AccountDA.CurrentUserID != 0)
                {
                    if (AccountDA.CurrentICOS >0)
                    {
                        List<SubmittedFilesViewModel> subFiles = await iDataStorageService.GetSubmittedFilesAsync(siteid);
                        return Json(subFiles);
                    }
                    else
                        return RedirectToAction("ChangePassword", "Account");
                }
                else
                    return RedirectToAction("Login", "Account");
            }
        /**/
        [HttpGet]
        public async Task<IActionResult> DownloadSubmittedFile(int ID)
        {
            if (AccountDA.CurrentUserID != 0)
            {
                string url = await iDataStorageService.DownloadSubmittedFile(/*70, */ID);
                return Json(url);
            }
            else
                return RedirectToAction("Login", "Account");
        }
         
        #region badm
        /*     public ActionResult Badm()
             {
                 if (AccountDA.CurrentUserId != null && AccountDA.CurrentUserId != 0)
                 {
                     if (AccountDA.CurrentICOS == true)
                         //TODO
                         //return View(PiDA.GetTimelineEventsAndAlerts(70));
                         return View();
                     else
                         return RedirectToAction("ChangePassword", "Account");
                 }
                 else
                     return RedirectToAction("Login", "Account");
             }
             */
        #endregion
        #region file upload
        /*
    public ActionResult Fileupload()
    {
        if (AccountDA.CurrentUserId != null && AccountDA.CurrentUserId != 0)
        {
            if (AccountDA.CurrentICOS == true)
            {
                //TODO
                //return View(PiDA.GetTimelineEventsAndAlerts(70));
                List<SelectListItem> items = new List<SelectListItem>();

                using (ICOSEntities context = new newIcos.ICOSEntities())
                {
                    var xitems = context.IcosSitePIS.Where(v => v.id_user == AccountDA.CurrentUserId).OrderBy(s => s.site_code).ToList();
                    foreach (var item in xitems)
                    {
                        items.Add(new SelectListItem { Text = item.site_code, Value = item.id_site.ToString() });
                    }
                }

                //                    items.Add(new SelectListItem { Text = "site1", Value = "0" });
                ViewBag.pisites = items.OrderBy(x=>x.Value);
                return View();
            }
            else
                return RedirectToAction("ChangePassword", "Account");
        }
        else
            return RedirectToAction("Login", "Account");
    }

    [HttpGet]
    public ActionResult UploadFile()
    {
        return View();
    }
    [HttpPost]
    public ActionResult UploadFile(HttpPostedFileBase file)
    {
        try
        {
            if (file.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(file.FileName);
                string _path = Path.Combine(Server.MapPath("~/tempupload"), _FileName);
                file.SaveAs(_path);
            }
            ViewBag.Message = "File Uploaded Successfully!!";
            return View();
        }
        catch
        {
            ViewBag.Message = "File upload failed!!";
            return View();
        }
    }
    */
        #endregion

        #region fieldbook

        public async Task<IActionResult> Fieldbook()
        {
            if (AccountDA.CurrentUserID != 0)
            {
                if (AccountDA.CurrentICOS == 1) { 
                    //TODO
                    //return View(PiDA.GetTimelineEventsAndAlerts(70));
                    ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
                    viewModel = await GetPISitesListAsync();
                    return View(viewModel);
                }
                else
                    return RedirectToAction("ChangePassword", "Account");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<string> Fbook(List<IFormFile> files, string fileType, string data_info, string site, string note_info)
        {
            var _root = configuration.GetSection("DefaultUploadFolder").Value;
            int piSession = AccountDA.CurrentUserID;
            string message = "", fullPath="";
            DateTime dt = DateTime.Now;
            string prefix = "";
           // context = new ICOSEntities();
            int fType = int.Parse(fileType);
            if (files == null || files.Count == 0)
            {
                //Process text file: create, save and submit
                
            }
            else
            {

            }
            string fileName = "";// files[0].FileName;
            switch (fType)
            {
                case 15:
                    message += "Text file ";
                    prefix = site + "_FBT_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + ".txt";
                    fullPath = Path.Combine(_root, fileName);
                    using (var wrFile = new StreamWriter(fullPath))
                    {
                        wrFile.WriteLine(note_info);
                    }
                    break;
                case 21:

                    prefix = site + "_FBA_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + files[0].FileName.Substring(files[0].FileName.LastIndexOf("."));
                    message += "Audio file ";
                    break;
                case 22:
                    prefix = site + "_FBP_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + files[0].FileName.Substring(files[0].FileName.LastIndexOf("."));
                    fullPath = Path.Combine(_root, fileName);
                  //  files[0].CopyTo(new FileStream(fullPath, FileMode.Create));

                    using (FileStream fs = System.IO.File.Create(fullPath))
                    {
                        files[0].CopyTo(fs);
                        fs.Flush();
                    }

                    message += "Image file " + files[0].FileName;
                    break;
            }
            /* if (fType != 15)
             {
                 //files[0].
                 int res = await xup2Async(files[0], site + "/fieldbook", fileName);
                 if (res == 0)
                 {
                     message = files[0].FileName + " correctly uploaded and renamed in " + fileName;
                 }
             }*/

            int pp = await xup2Async(fileName, site, null, true);

            int siteID = 0;
            short? siteClass = 0;
            iDataStorageService.GetSiteIdClassBySiteCode(site, ref siteID, ref siteClass);
            bool iRes= await iDataStorageService.SaveFileInFieldbookAsync(fileName, data_info, siteID, fType, AccountDA.CurrentUserID);
            if (!iRes)
            {
                message = "An error occurred in Fieldbook saving. Please contact administration (info@icos-etc.eu)";
            }
            else
            {
                message += " correctly uploaded and renamed in " + fileName;
            }
            return message;
        }

        public async Task<JsonResult> GetFieldbookData(int id)
        {
            if (id == 0)
            {
                return null;
            }
           
            List<FieldBookViewModel> rList = await iDataStorageService.GetFieldBookVMItems(id); //results.ToList();

            return Json(rList);
        }

        #endregion

        #region test jsgrid!!!
        [HttpPost]
        public JsonResult getData()
        {
            return null; // Json(tmList);
        }


            /*
    [HttpPost]
    public JsonResult getData(TeamModelFilter filter)
    {
        TeamMemberAdapter tma = new TeamMemberAdapter();
        try
        {
            int site = -1;
            // TODO: AC - wrap session in a property??!!!

            //filter.SITE = util.GetCurrentSelectedSite();
        }
        catch (Exception)
        {

            // TODO: AC - what to do ??
        }
        var tmList = tma.Adapt(new TeamModel().GetData(filter));
        // filters
        List<string> xparm = new List<string>();
        Type myType = filter.GetType();
        IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
        int contaparms = 0;
        string xdynque = @"";
        foreach (PropertyInfo prop in props)
        {
            if (prop.Name != "SITE")
            {
                object propValue = prop.GetValue(filter, null);
                if (propValue != null)
                {
                    // add to filer string
                    //xdynque = prop.Name.Replace("_DESCRIPTION", "") + @".Contains(@" + contaparms.ToString() + ") AND ";
                    xdynque = xdynque+ prop.Name + @".Contains(@" + contaparms.ToString() + ") AND ";
                    // 
                    xparm.Add(propValue.ToString());

                    contaparms++;
                }
            }

            // Do something with propValue
        }


        if (xparm.Count > 0)
        {
            xdynque = xdynque.Substring(0, xdynque.Length - 5);
            tmList = tmList.Where(xdynque, xparm.ToArray()).ToList();
        }
        return Json(tmList);
    }
    [HttpPost]
    public PartialViewResult GetdlgInputTeam(TeamModelUI tm, string teamname)
    {
        Uri ur = HttpContext.Request.Url;
        if (null == tm || null == tm.TEAM_MEMBER_NAME)
        {
            tm = new TeamModelUI();
            tm.TEAM_MEMBER_DATE = string.Empty;
            ViewBag.OperationRequest = "Create";
        }
        else
        {
            ViewBag.OperationRequest = "EditTeam";
        }
        //tm.Contracts = tm.GetContracts();
        //tm.Roles = tm.GetRoles();
        //tm.MainExperts = tm.GetMainExperts();
        ViewBag.Contracts = tm.GetContracts();
        ViewBag.Roles = tm.GetRoles();
        ViewBag.MainExperts = tm.GetMainExperts();
        //ViewBag.Percs=tm.GetPercs();
        return PartialView("/Views/PI/BadmDialog/_dialogInputTeam.cshtml", tm);
    }
    // GET: /Home/Create
    [HttpPost]
    public JsonResult Create(TeamModelUI input)
    {
        OperationResponse res = new OperationResponse { code = 0, message = "Add complete" };
        if (this.ModelState.IsValid)
        {



            try
            {
                TeamModel tm = new TeamModel();
                tm = new TeamMemberAdapter().Adapt(input);
                string validateReturn = new ServerValidator().ValidateTeam(tm);
                if (validateReturn.Length == 0)
                {

                    tm.Add(tm, AccountDA.CurrentUserId, util.GetCurrentSelectedSite(), 2);

                }
                else
                {
                    res.code = 200;
                    res.message = validateReturn;

                }


            }
            catch (Exception)
            {
                res.code = 200;
                res.message = "Database error";
            }
            return Json(res);
        }
        else
        {
            res.code = 100;
            res.message = "Model is not valid";
        }
        return Json(res);
    }

    private string ServerValidate(TeamModel input)
    {
        return "";
    }

    //
    [HttpPost]
    public JsonResult Delete(TeamModelUI tmui)
    {
        OperationResponse res = new OperationResponse { code = 0, message = "Record deleted" };
        try
        {
            TeamModel tm = new TeamModel();
            tm.Delete(new TeamMemberAdapter().Adapt(tmui), AccountDA.CurrentUserId);
        }
        catch
        {
        }
        return Json(res);
    }
    [HttpPost]
    public JsonResult EditTeam(TeamModelUI tmui)
    {
        OperationResponse res = new OperationResponse { code = 0, message = "" };
        if (ModelState.IsValid)
        {
            try
            {
                int xsite = 0;
                // TODO: check site ( refactor to function..)
                if (System.Web.HttpContext.Current.Session["PiSite"] != null)
                {
                    int.TryParse(System.Web.HttpContext.Current.Session["PiSite"].ToString(), out xsite);
                }
                if (xsite > 0)
                {
                    TeamModel tm = new TeamModel();
                    tm = new TeamMemberAdapter().Adapt(tmui);
                    string validateReturn = new ServerValidator().ValidateTeam(tm);
                    if (validateReturn.Length == 0)
                    {
                        tm.Update(tm, AccountDA.CurrentUserId, xsite, 2);
                    }
                    else
                    {
                        res.code = 200;
                        res.message = validateReturn;
                    }
                }
                else
                {
                    TempData["displaymessage"] = "Please select a valid site";
                    TempData["details"] = "";
                }
            }
            catch (Exception ex)
            {
                TempData["displaymessage"] = "Unable to edit values";
                TempData["details"] = ex.ToString();
            }
        }
        else
        {
            res.code = 50;
            res.message = "Please check invalid fields";
        }
        return Json(res);
    }
    [HttpPost]
    public ActionResult Update(DhpImagesModelUI dhpiui)
    {
        if (ModelState.IsValid)
        {
            try
            {
                DhpImagesModel dhpim = new DhpImagesModel();
                dhpim.Update(new DhpImagesAdapter().Adapt(dhpiui), AccountDA.CurrentUserId, util.GetCurrentSelectedSite(), 2);
            }
            catch (Exception ex)
            {
                TempData["displaymessage"] = "Unable to edit values";
                TempData["details"] = ex.ToString();
            }
        }
        return RedirectToAction("Test");
    }

    public ActionResult PartialTest<T>(T model)
    {
        return View();
    }
    */
            #endregion

            /*
    [HttpGet]
    public JsonResult GetCsvData(string xgroup)
    {
        String dataResult = "";
        int xId_site = -1;
        int xId_group = -1;
        if (System.Web.HttpContext.Current.Session["PiSite"] != null)
        {
            Int32.TryParse(System.Web.HttpContext.Current.Session["PiSite"].ToString(), out xId_site);
            Int32.TryParse(xgroup, out xId_group);
        }
        if (xId_site != -1 && xId_group != -1)
        {
            using (DB.DB db = new DB.DB("connMulti"))
            {

                string site = System.Web.HttpContext.Current.Session["PiSite"].ToString();

                SqlParameter[] pars = { new SqlParameter("siteId", xId_site), new SqlParameter("grId", xId_group) };
                SqlDataReader rd = db.ExecuteReaderStored("SelectDataGeneral", pars);

                String allData = "";
                for (int i = 0; i < rd.FieldCount; i++)
                {
                    if (i == 0)
                    { allData = allData + "\"" + "id" + "\";"; }
                    else if ((i > 0 && i < 6) || i==rd.FieldCount-1)  // cut non necessary fields
                    {
                    }
                    else
                    {
                        allData = allData + "\"" + rd.GetName(i) + "\";";
                    }
                }
                allData = allData + Environment.NewLine;
                // List<M> resList = new List<M>();
                while (rd.Read())
                {
                    for (int i = 0; i < rd.FieldCount; i++)
                    {
                        if ((i > 0 && i < 6) || i == rd.FieldCount - 1)  // cut non necessary fields
                        {
                        }
                        else
                        {
                            allData = allData + "\"" + rd[i].ToString() + "\";";
                        }


                    }
                    allData = allData + Environment.NewLine;

                    //var model = Activator.CreateInstance<M>();
                    //   foreach (PropertyInfo pi in model.GetType().GetFilteredProperties())
                    //   {
                    //       if (rd[pi.Name] != DBNull.Value) pi.SetValue(model, rd[pi.Name]);
                    //   }
                    //   int xid = 0;
                    //   Int32.TryParse(rd["id_datastorage"].ToString(), out xid);
                    ////   model.GetType().GetProperty("id_datastorage").SetValue(model, xid);
                    //   int xsd = 0;
                    //   Int32.TryParse(rd["dataStatus"].ToString(), out xsd);
                    //   model.GetType().GetProperty("dataStatus").SetValue(model, xsd);
                    //   resList.Add(model);
                }
                rd.Close();
                // open file on torage and write to it
                String xname = xId_site.ToString() + "_" + xId_group.ToString() + ".csv";
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                CloudFileShare share = fileClient.GetShareReference("icosshare");

                if (share.Exists())
                {
                    CloudFileDirectory rootDir = share.GetRootDirectoryReference();
                    CloudFileDirectory icosDir = rootDir.GetDirectoryReference("icos");
                    if (icosDir.Exists())
                    {
                        icosDir.GetFileReference(xname).UploadText(allData);
                        CloudFile cloudFile;
                        cloudFile = icosDir.GetFileReference(xname);
                        if (cloudFile.Exists())
                        {
                            var policy = new SharedAccessFilePolicy
                            {
                                Permissions = SharedAccessFilePermissions.Read,
                                SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
                                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1)
                            };
                            var url = cloudFile.Uri.AbsoluteUri + cloudFile.GetSharedAccessSignature(policy);

                            //Response.Redirect(url);
                            return Json(url, JsonRequestBehavior.AllowGet);
                        }
                    }
                }


            }
        }

       return Json("");
    }
    */
            private string SetNewFileName(string fname)
        {
            string newName = fname.Substring(0, fname.LastIndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fname.Substring(fname.LastIndexOf("."));
            return newName;
        }
        /*
        private void getCooFile(int ids, string ftj)
        {
            StreamWriter wr = new StreamWriter(ftj);
            wr.WriteLine("FEAT\tLAT\tLON\tPLOT_EASTWARD_DIST\tPLOT_NORTHWARD_DIST\tPLOT_DISTANCE_POLAR\tPLOT_ANGLE_POLAR\tPLOT_RECT_X\tPLOT_RECT_Y\tPLOT_RECT_DIR\tPLOT_COMMENT");
            var cpLatLon=context.DataStorages.Where(x=>x.groupID==3 && x.dataStatus==0 && x.siteID==ids).Select(x => new { x.qual0, x.qual1 }).SingleOrDefault();
            wr.WriteLine("TOWER" + "\t" + cpLatLon.qual0.ToString() + "\t" + cpLatLon.qual1.ToString() + "\t\t\t\t\t\t\t\t");
            var cpData = context.DataStorages.Where(x => x.groupID == 3 && x.dataStatus == 0 && x.siteID == ids).Select(x => new { x.qual0, x.qual7, x.qual8,
                                                                                                                                    x.qual2, x.qual3,x.qual4, x.qual5,
                                                                                                                                    x.qual11, x.qual12,x.qual13, x.qual9}).ToList();
            foreach (var cp in cpData)
            {
                wr.WriteLine(cp.qual0.ToString() + "\t" + cp.qual7.ToString() + "\t" + cp.qual8.ToString()+ "\t" + cp.qual2.ToString() + "\t" + cp.qual3.ToString() 
                                + "\t" + cp.qual4.ToString() + "\t" +cp.qual5.ToString() +  "\t" +cp.qual11.ToString() + "\t" + cp.qual12.ToString() + "\t" 
                                + cp.qual13.ToString() + "\t" + cp.qual9.ToString());
            }

            wr.Close();

        }*/
}
}