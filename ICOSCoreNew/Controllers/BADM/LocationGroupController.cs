﻿using System.Threading.Tasks;
using ICOSCoreNew.Models.BADM;
using ICOSCoreNew.Models.DTO;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.ViewModel;
using ICOSCoreNew.Models.ViewModel.BADM;
using Microsoft.AspNetCore.Mvc;
using static ICOSCoreNew.Models.Utils.Globals;

namespace ICOSCoreNew.Controllers.BADM
{
    public class LocationGroupController : Controller
    {
        private IVariableAdapter<LocationGroup> adapter;
        private readonly IDataStorageServices iDataStorageService;

        public LocationGroupController(IVariableAdapter<LocationGroup> _adapter, IDataStorageServices _ids)
        {
            adapter = _adapter;
            iDataStorageService = _ids;
        }

        public async Task<IActionResult> Index()
        {
            //ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
            //viewModel = await GetPISitesListAsync();
            ICOSSitePiViewModel viewModel = await iDataStorageService.GetPISitesViewModelListAsync();
            // return View(viewModel);
            return View("/Views/BADM/LocationGroup.cshtml", viewModel);
           // return View("BADM/Index");
        }

        [HttpPost]
        public JsonResult getData(LocationGroupViewModel location)
        {
            adapter.GetVariableFromDataStorage(location.SiteId, (int)BadmVarsGroups.GRP_LOCATION);
            return null;//base.getData<LocationGroupModel, LocationGroupAdapter, LocationGroupFilter, LocationGroupUI>(filter);
        }
    }
}