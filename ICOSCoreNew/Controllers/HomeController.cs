﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ICOSCoreNew.Models;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.ViewModel;
using Microsoft.AspNetCore.Http;

namespace ICOSCoreNew.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDataStorageServices service;

        public HomeController(IDataStorageServices service)
        {
            this.service = service;
        }
        public IActionResult Index()
        {
            HttpContext.Session.SetString("aaa", "Jarvik");
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Documents()
        {
            return View();
        }

        public IActionResult People()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region ajax

        [HttpPost]
        public async Task<ActionResult> GetIcosSites()
        {
            List<SiteViewModel> result = new List<SiteViewModel>();
            result = await service.GetIcosSitesAsync();
            return Json(result);
        }
        #endregion
    }
}
