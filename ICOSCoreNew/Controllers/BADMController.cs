﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICOSCoreNew.DB;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace ICOSCoreNew.Controllers
{
    public class BADMController : Controller
    {
        private readonly IDataStorageServices iDataStorageService;

        public BADMController(IDataStorageServices ids)
        {
            iDataStorageService = ids;
        }

        public async Task<IActionResult> Index()
        {
            if (AccountDA.CurrentUserID != 0)
            {
                if (AccountDA.CurrentICOS == 1)
                { 
                    //TODO
                    //return View(PiDA.GetTimelineEventsAndAlerts(70));
                    ICOSSitePiViewModel viewModel = await iDataStorageService.GetPISitesViewModelListAsync();
                    return View(viewModel);
                }
                else
                    return RedirectToAction("ChangePassword", "Account");
            }
            else
                return RedirectToAction("Login", "Account");
        }

        public IActionResult Team()
        {
            return View();
        }

        [HttpPost]
        public JsonResult getData()
        {
            return null; // Json(tmList);
        }
    }
}