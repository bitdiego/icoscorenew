﻿//using newIcos.DB;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ICOSCoreNew.Models;
using ICOSCoreNew.DB;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using System.Threading.Tasks;

namespace ICOSCoreNew.Controllers
{
    public class AccountController : Controller
    {
  //      private readonly RequestHandler requestHandler;
        private readonly IUserLoginServices iUserloginServices;

        public AccountController(/*RequestHandler _requestHandler, */IUserLoginServices _iuserLogInServ)
        {
    //        this.requestHandler = _requestHandler;
            this.iUserloginServices = _iuserLogInServ;
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public IActionResult Login(LoginModel model)
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await iUserloginServices.CheckLoginAsync(model);
                if(user is null)
                {
                    ModelState.AddModelError("", "Username and password do not match. Please try again or use the password forgotten function.");
                    return View(model);
                }
                else
                {
                    if (user.first_acc==0)
                    {
                        //Session["ICOS"] = 1;
                        return RedirectToAction("Dashboard", "PI");
                    }
                    else
                    {
                        // Session["ICOS"] = 0;
                        return RedirectToAction("ChangePassword", "Account");
                    }
                }
            }
            /*if (ModelState.IsValid)
            {
                AccountDA account = new AccountDA();
                UserModel user = account.Login(model);
                if (user != null)
                {
                    //Session["userName"] = user.userName;
                    //Session["userID"] = user.userID;
                    if (!user.firstAccess)
                    {
                        //Session["ICOS"] = 1;
                        return RedirectToAction("Dashboard", "PI");
                    }
                    else
                    {
                       // Session["ICOS"] = 0;
                        return RedirectToAction("ChangePassword", "Account");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username and password do not match. Please try again or use the password forgotten function.");
                    return View(model);
                }
            }*/
            return View(model);
        }

        public ActionResult ChangePassword(string returnUrl)
        {
            if ( AccountDA.CurrentUserID != 0)
            {
                if (AccountDA.CurrentICOS == 0)
                    return View();
                else
                {
                    AccountDA.CurrentICOS = 1;
                    return RedirectToAction("Dashboard", "PI");
                    
                }
            }
            else
                return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model, string returnUrl)
        {
            /*if (ModelState.IsValid)
            {
                if (AccountDA.CurrentUserID != 0)
                {
                    model.userID = AccountDA.CurrentUserID;
                    AccountDA.ChangePassword(model);
                    AccountDA.CurrentICOS = 1;
                    return RedirectToAction("Dashboard", "PI");
                }
                else
                    return RedirectToAction("Login", "Account");
            }*/
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string returnUrl)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model, string returnUrl)
        {
           /* if (ModelState.IsValid)
            {
                model = AccountDA.ResetPassword(model);
                if (!model.existingEmail.Value)
                {
                    ModelState.AddModelError("", "Entered email not found. Please try again or contact administration.");
                    return View(model);
                }
                else
                    return RedirectToAction("Login", "Account");

            }*/
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AccountDA.CurrentUserID = 0;
            AccountDA.CurrentUserName = null;
            AccountDA.CurrentICOS = 0;

            /*Session["userName"] = null;
            Session["userID"] = null;
            Session["ICOS"] = null;
            */
            return RedirectToAction("Index", "Home");
        }

        public ActionResult ChallengeResult(string provider, string Url, string userId)
        {
            return null;
        }

    }
}