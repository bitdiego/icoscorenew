﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ICOSCoreNew.Models.Entities;
using ICOSCoreNew.Models.SqlDbServices;
using ICOSCoreNew.Models.UI;
using ICOSCoreNew.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ICOSCoreNew.Controllers
{
    public class SiteSelectorController : Controller
    {
        private readonly IDataStorageServices iDataStorageService;
        private readonly ISitesListService iSitesListService;

        public SiteSelectorController(IDataStorageServices _ids, ISitesListService _iss)
        {
            this.iDataStorageService = _ids;
            this.iSitesListService = _iss;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> SiteSelect()
        {
            List<IcosSitePI> _tList = await iDataStorageService.GetPISitesListAsync();
            ICOSSitePiViewModel viewModel = new ICOSSitePiViewModel();
            viewModel.sitesPiList = new List<SelectListItem>();

            foreach(var _t in _tList)
            {
                viewModel.sitesPiList.Add(new SelectListItem(_t.site_code, _t.id_site.ToString()));
            }

            return View(viewModel);
        }
    }
}